# **Vedanta Vidiyalay**

This app is use to manage daily managements task of any middle level school. Since this application is still in beta version and we are still working on it. we need your help to raise issues, or new feature that you want us to include.


## **What is this repository for?**

- This application includes below features -
- Create new student data, and update student data
- Print scholler reports
- Create and maintain student fee master, and fee records
- Create and maintain fine records

##**How do I get set up?**
###Summary of set up
####Docker-compose
You can run the application using docker compose
`docker-compose up `
####Local set up
#####Setup keycloak
- Check this short video how to setup keycloak for this application
**http://bit.ly/vikaskeycloaksetup**
#####Urls
- keycloak: http://localhost:9001/auth
- application: http://localhost:8080

To run the application locally you need to run the kafka server, keycloak, and database from below docker compose file.
`docker-compose -f  local.yml up`
Go to vedanta-school-backend directory and maven clean install
`mvn clean install`
####Run frontend separatly
go to vedanta-school-frontend dire
`npm install`
`ng serve -o`

##Dependencies
- JDK8
- Docker version 18.03.1-ce
- Spring-boot:2.0.6.RELEASE
- Spring Security
- Keycloak:4.5.0.Final
- Confluent Kafka
- MySQL: 5.6.34

##Database configuration
Connect Url: jdbc:mysql://localhost:3305/app-db
username: root
password:
(empty password)


##Who do I talk to?
###Repository owner or admin
####**vikas.on.app@gmail.com**