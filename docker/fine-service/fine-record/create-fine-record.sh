

export TOKEN=`curl -ss --data "grant_type=password&client_id=sample-api&username=vikas&password=vikas" \
    http://localhost:9001/auth/realms/sample-realm/protocol/openid-connect/token | jq -r .access_token`
echo $TOKEN

#curl -V -XPOST -H "Authorization: bearer $TOKEN" -H 'Content-Type: application/json' 'http://localhost:8080/api/fine/create-update-fine-master/new' -H 'Content-Type: application/json'  --data-binary '{"fineType":1,"amount":20, "standard":"1"}' --compressed


curl -v -XPOST -H "Authorization: Bearer $TOKEN" \
    -H "Content-type: application/json" \
    -d '{"id":3701013220250343956,"enrolmentNo":35,"fineType":"FEE_DUE","description":"NSVCZPl","status":"ACTIVE"}' \
    'http://localhost:8080/api/update-fine-record/create'
