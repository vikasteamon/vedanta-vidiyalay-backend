#!/usr/bin/env bash

export TOKEN=`curl -ss --data "grant_type=password&client_id=sample-api&username=vikas&password=vikas" \
    http://localhost:9001/auth/realms/sample-realm/protocol/openid-connect/token | jq -r .access_token`
echo $TOKEN

#
curl -ss -XGET -H "Authorization: Bearer $TOKEN" -H "Content-type: application/json" \
    'http://localhost:8080//api/fee_due/details'

#curl -ss -XGET -H "Authorization: Bearer $TOKEN" -H "Content-type: application/json" \
#    'http://localhost:8080/api/query-fine-record/fineType/FEE_DUE'

#curl -ss -XGET -H "Authorization: Bearer $TOKEN" -H "Content-type: application/json" \
#    'http://localhost:8080/api/query-fine-record/enrolmentNo/12'
