#!/usr/bin/env bash

export TOKEN=`curl -ss --data "grant_type=password&client_id=sample-api&username=vikas&password=vikas" \
    http://localhost:9001/auth/realms/sample-realm/protocol/openid-connect/token | jq -r .access_token`
echo $TOKEN



curl -v -XPOST -H "Authorization: Bearer $TOKEN" -H "Content-type: application/json" -d '{"id":10,"fineType":"FEE_DUE","amount":30, "standard":1}' 'http://localhost:8080/api/fine/create-update-fine-master/update'