#!/usr/bin/env bash

export TOKEN=`curl -ss --data "grant_type=password&client_id=sample-api&username=vikas&password=vikas" \
    http://localhost:9001/auth/realms/sample-realm/protocol/openid-connect/token | jq -r .access_token`
echo $TOKEN


curl -v -XGET -H "Authorization: Bearer $TOKEN" -H "Content-type: application/json" \
    'http://localhost:8080/api/query-fee-master/findAll'

curl -v -XGET -H "Authorization: Bearer $TOKEN" -H "Content-type: application/json" \
   'http://localhost:8080/api/fineType/FEE_DUE/standard/1'