
export TOKEN=`curl -ss --data "grant_type=password&client_id=sample-api&username=vikas&password=vikas" \
    http://localhost:9001/auth/realms/sample-realm/protocol/openid-connect/token | jq -r .access_token`
echo $TOKEN

curl -ss -H "Authorization: bearer $TOKEN" \
    'http://localhost:8080/api/fee-due-recalculater-resource/recalculate' \
    -H 'Content-Type: application/json'