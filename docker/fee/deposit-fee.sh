#!/usr/bin/env bash
export TOKEN=`curl -ss --data "grant_type=password&client_id=sample-api&username=vikas&password=vikas" \
    http://localhost:9001/auth/realms/sample-realm/protocol/openid-connect/token | jq -r .access_token`
#echo $TOKEN

curl  -H "Authorization: bearer $TOKEN" 'http://localhost:8080/api/fee-deposit/deposit' \
-H 'Content-Type: application/json' \
--data-binary '{"enrolmentNo":1,"amount":10000.00,"instrumentNo":"1234","transactionMode":"CASH"}' --compressed
