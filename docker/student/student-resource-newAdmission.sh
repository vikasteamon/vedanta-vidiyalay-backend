

export TOKEN=`curl -ss --data "grant_type=password&client_id=sample-api&username=vikas&password=vikas" \
    http://localhost:9001/auth/realms/sample-realm/protocol/openid-connect/token | jq -r .access_token`
#echo $TOKEN

curl -ss -H "Authorization: bearer $TOKEN" \
    -H 'Content-Type: application/json' 'http://localhost:8080/api/student-resource/newAdmission' \
    --data-binary '{"name":"vikas","dateOfBirth":"1984-08-24","religion":"hindu","caste":"hindi","subcaste":"hindi","gender":"MALE","fatherName":"SUBHASH","motherName":null,"occupation":null,"relation":null,"address1":"ADD1","pin1":"222134","address2":null,"pin2":null,"phone1":null,"phone2":null,"mobile":"98","email":"vikasonapp@gmail.com","bloodGroup":null,"chechak":null,"nationality":null,"motherTongue":null,"areaOfInterest":null,"numberOfSiblings":null,"lastSchoolName":null,"lastRank":null,"lastClassYear":null,"lastClass":null,"board":null,"scored":null,"totalMarks":null,"reasonForLeave":null,"transferCertificate":"Yes","lastSchoolMarkSheet":null,"conduct":null,"admissionClass":1,"currentClass":null,"instrumentNo":"1234"}'

#
#curl -ss 'http://localhost:8080/api/student-resource/newAdmission' \
#-H 'Origin: http://localhost:4200' -H 'Accept-Encoding: gzip, deflate, br' \
#-H 'Accept-Language: en-US,en;q=0.9,hi;q=0.8,th;q=0.7' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36' -H 'Content-Type: application/json' -H 'Accept: application/json, text/plain, */*' -H 'Referer: http://localhost:4200/new-admission-form' -H 'Connection: keep-alive' -H 'DNT: 1' \
#--data-binary '{"name":"vikas","dateOfBirth":"1984-08-24","religion":"hindu","caste":"hindi","subcaste":"hindi","gender":"MALE","fatherName":"SUBHASH","motherName":null,"occupation":null,"relation":null,"address1":"ADD1","pin1":"222134","address2":null,"pin2":null,"phone1":null,"phone2":null,"mobile":"98","email":"vikasonapp@gmail.com","bloodGroup":null,"chechak":null,"nationality":null,"motherTongue":null,"areaOfInterest":null,"numberOfSiblings":null,"lastSchoolName":null,"lastRank":null,"lastClassYear":null,"lastClass":null,"board":null,"scored":null,"totalMarks":null,"reasonForLeave":null,"transferCertificate":"Yes","lastSchoolMarkSheet":null,"conduct":null,"admissionClass":1,"currentClass":null,"instrumentNo":"1234"}' --compressed


#CREATE TABLE `jhi_persistent_audit_evt_data` (
#  `event_id` bigint(20) NOT NULL AUTO_INCREMENT,
#  `name` varchar(255) DEFAULT NULL,
#  `value` varchar(255) NOT NULL,
#  PRIMARY KEY (`event_id`)
#) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;