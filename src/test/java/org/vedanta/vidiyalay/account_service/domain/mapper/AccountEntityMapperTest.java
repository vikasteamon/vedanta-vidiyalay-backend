package org.vedanta.vidiyalay.account_service.domain.mapper;

import io.github.benas.randombeans.api.EnhancedRandom;
import org.junit.Assert;
import org.junit.Test;
import org.vedanta.vidiyalay.account_service.domain.AccountMasterEntity;
import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountMasterVM;

public class AccountEntityMapperTest {
    AccountEntityMapper accountEntityMapper = new AccountEntityMapperImpl();

    @Test
    public void testToEntity() throws Exception {
        final AccountMasterVM accountMasterVM = EnhancedRandom.random(AccountMasterVM.class);
        final AccountMasterEntity accountMasterEntity = accountEntityMapper.toEntity(accountMasterVM);
        Assert.assertNotNull(accountMasterEntity);
        Assert.assertNotNull("id is null", accountMasterEntity.getId());

    }
}



//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme