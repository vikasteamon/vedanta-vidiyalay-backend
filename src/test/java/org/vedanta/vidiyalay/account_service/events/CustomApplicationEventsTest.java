package org.vedanta.vidiyalay.account_service.events;

import org.junit.Assert;
import org.junit.Test;

public class CustomApplicationEventsTest {
    //Field eventType of type EventType - was not mocked since Mockito doesn't mock enum

    @Test
    public void testGetSourceFromEvent() throws Exception {
        CustomApplicationEvents customApplicationEvents = new CustomApplicationEvents("dummy", EventType.STUDENT_TERMINATION_PROCESS_STARTED);
        String result = customApplicationEvents.getSourceFromEvent(EventType.STUDENT_TERMINATION_PROCESS_STARTED, String.class);
        Assert.assertTrue(result instanceof String);
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme