package org.vedanta.vidiyalay.account_service.facade.impl;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.vedanta.vidiyalay.config.ApplicationProperties;
import org.vedanta.vidiyalay.email_service.SendEmailNotification;
import org.vedanta.vidiyalay.student_service.domain.enums.AdmissionStatus;
import org.vedanta.vidiyalay.student_service.domain.enums.Gender;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;

import java.util.Calendar;
import java.util.GregorianCalendar;

import static org.mockito.Mockito.when;

public class ExceptionMailerTest {
    @Mock
    private SendEmailNotification sendEmailNotification;
    @Mock
    private ApplicationProperties applicationProperties;
    @InjectMocks
    private ExceptionMailer exceptionMailer;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testSendExceptionDetails() throws Exception {
        when(applicationProperties.getExceptionHandler()).thenReturn(new ApplicationProperties.ExceptionHandler());

        exceptionMailer.sendExceptionDetails(new StudentNewAdmissionVM(Long.valueOf(1), "name", new GregorianCalendar(2019, Calendar.MAY, 10, 10, 11).getTime(), new GregorianCalendar(2019, Calendar.MAY, 10, 10, 11).getTime(), new GregorianCalendar(2019, Calendar.MAY, 10, 10, 11).getTime(), "religion", "caste", "subcaste", Gender.MALE, "fatherName", "motherName", "occupation", "relation", "address1", "pin1", "address2", "pin2", "phone1", "phone2", "mobile", "email", "bloodGroup", "chechak", "nationality", "motherTongue", "areaOfInterest", Integer.valueOf(0), "lastSchoolName", Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), "board", Integer.valueOf(0), Integer.valueOf(0), "reasonForLeave", "transferCertificate", "lastSchoolMarkSheet", "conduct", Integer.valueOf(0), Integer.valueOf(0), Boolean.TRUE, Boolean.TRUE, "instrumentNo", AdmissionStatus.PENDING_FOR_APPROVAL), "message");
    }
}
