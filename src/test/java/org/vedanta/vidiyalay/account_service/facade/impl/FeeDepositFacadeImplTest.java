/*
 *     Copyright (C) 2019  Vikas Kumar Verma
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.vedanta.vidiyalay.account_service.facade.impl;

import io.github.benas.randombeans.api.EnhancedRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.vedanta.vidiyalay.account_service.facade.QueryFeeDetailFacade;
import org.vedanta.vidiyalay.account_service.services.FeeDepositService;
import org.vedanta.vidiyalay.account_service.services.QueryFeeDetailService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountTransactionVo;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;

import java.math.BigDecimal;

import static org.mockito.Mockito.*;

public class FeeDepositFacadeImplTest {
    @Mock
    private FeeDepositService feeDepositService;

    @Mock
    private QueryFeeDetailService queryFeeDetailService;
    @InjectMocks
    private FeeDepositFacadeImpl feeDepositFacadeImpl;
    @Mock
    private QueryFeeDetailFacade queryFeeDetailFacade;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testDepositFee() throws Exception {

        when(feeDepositService.depositFee(any(), any(), any())).thenReturn(new BigDecimal(0));
        when(queryFeeDetailService.getTotalDueFee(any())).thenReturn(BigDecimal.ZERO);

        when(queryFeeDetailFacade.getTotalDueFee(anyLong()))
            .thenReturn(BigDecimal.ONE);

        when(queryFeeDetailFacade.getStudentDetailsVM(anyLong()))
            .thenReturn(EnhancedRandom.random(StudentNewAdmissionVM.class));


        BigDecimal result = feeDepositFacadeImpl.depositFee(new AccountTransactionVo(Long.valueOf(1), new BigDecimal(0), "instrumentNo", "transactionMode"));
        Assert.assertEquals(BigDecimal.ZERO, result);
    }
}
