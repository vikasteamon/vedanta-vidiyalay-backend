package org.vedanta.vidiyalay.account_service.facade.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.vedanta.vidiyalay.account_service.services.QueryFeeDetailService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.FeeDetailsVM;
import org.vedanta.vidiyalay.student_service.domain.enums.AdmissionStatus;
import org.vedanta.vidiyalay.student_service.domain.enums.Gender;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;

import java.math.BigDecimal;
import java.util.*;

import static org.mockito.Mockito.*;

public class QueryFeeDetailFacadeImplTest {
    @Mock
    QueryFeeDetailService queryFeeDetailService;
    @InjectMocks
    QueryFeeDetailFacadeImpl queryFeeDetailFacadeImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetStudentDetailsVM() throws Exception {
        when(queryFeeDetailService.getStudentDetailsVM(anyLong())).thenReturn(new StudentNewAdmissionVM(Long.valueOf(1), "name", new GregorianCalendar(2019, Calendar.MAY, 18, 10, 42).getTime(), new GregorianCalendar(2019, Calendar.MAY, 18, 10, 42).getTime(), new GregorianCalendar(2019, Calendar.MAY, 18, 10, 42).getTime(), "religion", "caste", "subcaste", Gender.MALE, "fatherName", "motherName", "occupation", "relation", "address1", "pin1", "address2", "pin2", "phone1", "phone2", "mobile", "email", "bloodGroup", "chechak", "nationality", "motherTongue", "areaOfInterest", Integer.valueOf(0), "lastSchoolName", Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), "board", Integer.valueOf(0), Integer.valueOf(0), "reasonForLeave", "transferCertificate", "lastSchoolMarkSheet", "conduct", Integer.valueOf(0), Integer.valueOf(0), Boolean.TRUE, Boolean.TRUE, "instrumentNo", AdmissionStatus.PENDING_FOR_APPROVAL));

        StudentNewAdmissionVM result = queryFeeDetailFacadeImpl.getStudentDetailsVM(Long.valueOf(1));
        Assert.assertEquals(new StudentNewAdmissionVM(Long.valueOf(1), "name", new GregorianCalendar(2019, Calendar.MAY, 18, 10, 42).getTime(), new GregorianCalendar(2019, Calendar.MAY, 18, 10, 42).getTime(), new GregorianCalendar(2019, Calendar.MAY, 18, 10, 42).getTime(), "religion", "caste", "subcaste", Gender.MALE, "fatherName", "motherName", "occupation", "relation", "address1", "pin1", "address2", "pin2", "phone1", "phone2", "mobile", "email", "bloodGroup", "chechak", "nationality", "motherTongue", "areaOfInterest", Integer.valueOf(0), "lastSchoolName", Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), "board", Integer.valueOf(0), Integer.valueOf(0), "reasonForLeave", "transferCertificate", "lastSchoolMarkSheet", "conduct", Integer.valueOf(0), Integer.valueOf(0), Boolean.TRUE, Boolean.TRUE, "instrumentNo", AdmissionStatus.PENDING_FOR_APPROVAL), result);
    }

    @Test
    public void testGetTotalDueFee() throws Exception {
        when(queryFeeDetailService.getTotalDueFee(anyLong())).thenReturn(new BigDecimal(0));

        BigDecimal result = queryFeeDetailFacadeImpl.getTotalDueFee(Long.valueOf(1));
        Assert.assertEquals(new BigDecimal(0), result);
    }

    @Test
    public void testGetFeeDetails() throws Exception {
        when(queryFeeDetailService.getFeeDetails(anyInt(), anyInt())).thenReturn(Arrays.<FeeDetailsVM>asList(new FeeDetailsVM(Long.valueOf(1), 0, 0, "description", new BigDecimal(0), Boolean.TRUE)));

        List<FeeDetailsVM> result = queryFeeDetailFacadeImpl.getFeeDetails(0, 0);
        Assert.assertEquals(Arrays.<FeeDetailsVM>asList(new FeeDetailsVM(Long.valueOf(1), 0, 0, "description", new BigDecimal(0), Boolean.TRUE)), result);
    }

    @Test
    public void testGetTotalFee() throws Exception {
        when(queryFeeDetailService.getTotalFee(anyInt(), anyInt())).thenReturn(null);

        Optional<BigDecimal> result = queryFeeDetailFacadeImpl.getTotalFee(0, 0);
        Assert.assertEquals(null, result);
    }

    @Test
    public void testCalculateDueFee() throws Exception {
        queryFeeDetailFacadeImpl.calculateDueFee(new FeeDetailsVM(Long.valueOf(1), 0, 0, "description", new BigDecimal(0), Boolean.TRUE));
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme