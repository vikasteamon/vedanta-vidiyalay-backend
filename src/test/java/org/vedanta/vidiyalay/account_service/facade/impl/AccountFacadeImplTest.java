package org.vedanta.vidiyalay.account_service.facade.impl;

import io.github.benas.randombeans.api.EnhancedRandom;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.vedanta.vidiyalay.account_service.enums.AccountType;
import org.vedanta.vidiyalay.account_service.facade.QueryFeeDetailFacade;
import org.vedanta.vidiyalay.account_service.services.AccountService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountMasterVM;
import org.vedanta.vidiyalay.student_service.domain.enums.AdmissionStatus;
import org.vedanta.vidiyalay.student_service.domain.enums.Gender;
import org.vedanta.vidiyalay.student_service.facade.QueryStudentFacade;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Optional;

import static org.mockito.Mockito.*;

public class AccountFacadeImplTest {
    @Mock
    private AccountService accountService;
    @Mock
    private QueryFeeDetailFacade queryFeeDetailFacade;
    @Mock
    private QueryStudentFacade queryStudentFacade;
    @InjectMocks
    private AccountFacadeImpl accountFacadeImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateNewAccount(){
        AccountMasterVM random = new AccountMasterVM(1L, "name", 1L, new GregorianCalendar(2019, Calendar.MAY, 10, 9, 24).getTime(), new BigDecimal(0), new BigDecimal(0), AccountType.STUDENT, new BigDecimal(0));
        final StudentNewAdmissionVM studentNewAdmissionVM = EnhancedRandom.random(StudentNewAdmissionVM.class);
        when(accountService.createNewAccount(any(StudentNewAdmissionVM.class), any())).thenReturn(random);

        when(queryStudentFacade.findByEnrolmentNoAndValidateAdmissionStatus(anyLong()))
                .thenReturn(Optional.of(studentNewAdmissionVM));

        when(queryFeeDetailFacade.getTotalFee(anyInt(), anyInt()))
                .thenReturn(Optional.of(BigDecimal.ONE));

        AccountMasterVM result = accountFacadeImpl.createNewAccount(random);
        Assert.assertEquals(random, result);
    }

    @Test
    public void testUpdateAccount() {
        final AccountMasterVM random = new AccountMasterVM(1L, "name", 1L, new GregorianCalendar(2019, Calendar.MAY, 10, 9, 24).getTime(), new BigDecimal(0), new BigDecimal(0),
                AccountType.STUDENT, new BigDecimal(0));
        when(accountService.updateAccount(any())).thenReturn(random);

        AccountMasterVM result = accountFacadeImpl.updateAccount(random);
        Assertions.assertThat(random)
                .isEqualTo(
                result);
    }

    @Test
    public void testGetAccountDetails() {
        final AccountMasterVM random = new AccountMasterVM(1L, "name", 1L, new GregorianCalendar(2019, Calendar.MAY, 10, 9, 24).getTime(), new BigDecimal(0), new BigDecimal(0), AccountType.STUDENT, new BigDecimal(0));
        when(accountService.getAccountDetails(anyLong())).thenReturn(random);

        AccountMasterVM result = accountFacadeImpl.getAccountDetails(Long.valueOf(1));
        Assert.assertEquals(random, result);
    }

    @Test
    public void testCreateAccountListener(){
        final StudentNewAdmissionVM studentNewAdmissionVM = new StudentNewAdmissionVM(1L, "name", new GregorianCalendar(2019, Calendar.MAY, 10, 9, 24).getTime(), new GregorianCalendar(2019, Calendar.MAY, 10, 9, 24).getTime(), new GregorianCalendar(2019, Calendar.MAY, 10, 9, 24).getTime(), "religion", "caste", "subcaste", Gender.MALE, "fatherName", "motherName", "occupation", "relation", "address1", "pin1", "address2", "pin2", "phone1", "phone2", "mobile", "email", "bloodGroup", "chechak", "nationality", "motherTongue", "areaOfInterest", Integer.valueOf(0), "lastSchoolName", Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), "board", Integer.valueOf(0), Integer.valueOf(0), "reasonForLeave", "transferCertificate", "lastSchoolMarkSheet", "conduct", Integer.valueOf(0), Integer.valueOf(0), Boolean.TRUE, Boolean.TRUE, "instrumentNo", AdmissionStatus.PENDING_FOR_APPROVAL);
        when(queryFeeDetailFacade.getTotalFee(anyInt(), anyInt()))
                .thenReturn(Optional.of(BigDecimal.ONE));
        doNothing().when(accountService).createAccountListener(studentNewAdmissionVM, BigDecimal.ONE);

        accountFacadeImpl.createAccountListener(studentNewAdmissionVM);
    }
    }
