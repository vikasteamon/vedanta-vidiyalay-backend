package org.vedanta.vidiyalay.account_service.facade.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.vedanta.vidiyalay.account_service.services.QueryFeesService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.FeeDetailsVM;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.when;

public class QueryFeesFacadeImplTest {
    @Mock
    QueryFeesService queryFeesService;
    @InjectMocks
    QueryFeesFacadeImpl queryFeesFacadeImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAll() throws Exception {
        when(queryFeesService.findAll()).thenReturn(Arrays.<FeeDetailsVM>asList(new FeeDetailsVM(Long.valueOf(1), 0, 0, "description", new BigDecimal(0), Boolean.TRUE)));

        List<FeeDetailsVM> result = queryFeesFacadeImpl.findAll();
        Assert.assertEquals(Arrays.<FeeDetailsVM>asList(new FeeDetailsVM(Long.valueOf(1), 0, 0, "description", new BigDecimal(0), Boolean.TRUE)), result);
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme