/*
 *     Copyright (C) 2019  Vikas Kumar Verma
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.vedanta.vidiyalay.account_service.facade.impl;

import io.github.benas.randombeans.api.EnhancedRandom;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.ApplicationEventPublisher;
import org.vedanta.vidiyalay.account_service.services.QueryFeeDetailService;
import org.vedanta.vidiyalay.account_service.services.UpdateFeeDetailService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.FeeDetailsVM;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FeeDetailsFacadeImplTest {

    @Mock
    private QueryFeeDetailService feeDetailServices;
    @Mock
    private UpdateFeeDetailService updateFeeDetailService;

    @Mock
    private ApplicationEventPublisher applicationEventPublisher;

    @InjectMocks
    private FeeDetailsFacadeImpl feeDetailsFacade;

    @Test
    public void getFeeDetails() {
        final List<FeeDetailsVM> expected = EnhancedRandom.randomListOf(10, FeeDetailsVM.class);
        when(feeDetailServices.getFeeDetails(1, 2019))
                .thenReturn(expected);

        List<FeeDetailsVM> actual = feeDetailsFacade.getFeeDetails(1, 2019);

        Mockito.verify(feeDetailServices).getFeeDetails(1, 2019);
    }

    @Test
    public void getTotalFee() {
        final Optional<BigDecimal> expected = Optional.of(BigDecimal.ONE);
        when(feeDetailServices.getTotalFee(1, 2019))
                .thenReturn(expected);

        Optional<BigDecimal> actual = feeDetailsFacade.getTotalFee(1, 2019);

        Mockito.verify(feeDetailServices).getTotalFee(1, 2019);

        Assertions.assertThat(actual.orElseGet(null)).isEqualTo(expected.orElseGet(null));
    }

    @Test
    public void createFeeDetail() {
        FeeDetailsVM feeDetailsVM = EnhancedRandom.random(FeeDetailsVM.class);

        when(updateFeeDetailService.createFeeDetail(feeDetailsVM))
                .thenReturn(feeDetailsVM);

        doNothing().when(applicationEventPublisher).publishEvent(ArgumentMatchers.any());

        FeeDetailsVM actual = feeDetailsFacade.createFeeDetail(feeDetailsVM);

        Mockito.verify(updateFeeDetailService).createFeeDetail(feeDetailsVM);

        Assertions.assertThat(actual).isEqualTo(feeDetailsVM);
    }
}