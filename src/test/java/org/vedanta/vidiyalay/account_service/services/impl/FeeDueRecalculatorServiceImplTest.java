package org.vedanta.vidiyalay.account_service.services.impl;

import io.github.benas.randombeans.api.EnhancedRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationEventPublisher;
import org.vedanta.vidiyalay.account_service.domain.AccountMasterEntity;
import org.vedanta.vidiyalay.account_service.facade.FeeDetailsFacade;
import org.vedanta.vidiyalay.account_service.repository.AccountMasterRepository;
import org.vedanta.vidiyalay.account_service.repository.AccountTransactionRepository;
import org.vedanta.vidiyalay.fine_service.domain.FineRecordEntity;
import org.vedanta.vidiyalay.fine_service.domain.enums.Status;
import org.vedanta.vidiyalay.fine_service.service.QueryFineRecordService;
import org.vedanta.vidiyalay.student_service.domain.StudentNewAdmissionEntity;
import org.vedanta.vidiyalay.student_service.domain.enums.AdmissionStatus;
import org.vedanta.vidiyalay.student_service.domain.enums.Gender;
import org.vedanta.vidiyalay.student_service.facade.QueryStudentFacade;
import org.vedanta.vidiyalay.student_service.facade.UpdateStudentDetailsFacade;
import org.vedanta.vidiyalay.student_service.mapper.StudentAdmissionDetailMapperImpl;
import org.vedanta.vidiyalay.student_service.repository.StudentNewAdmissionEntityRepository;
import org.vedanta.vidiyalay.student_service.service.QueryStudentService;
import org.vedanta.vidiyalay.student_service.service.UpdateStudentDetailsService;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Optional;

import static org.mockito.Mockito.*;

public class FeeDueRecalculatorServiceImplTest {
    @Mock
    AccountMasterRepository accountMasterRepository;
    @Mock
    AccountTransactionRepository accountTransactionRepository;
    @Mock
    StudentNewAdmissionEntityRepository studentNewAdmissionEntityRepository;
    @Mock
    FeeDetailsFacade feeDetailsFacade;
    @Mock
    QueryStudentFacade queryStudentFacade;
    @Mock
    UpdateStudentDetailsFacade updateStudentDetailsFacade;
    @Mock
    QueryFineRecordService queryFineRecordService;
    @Mock
    QueryStudentService queryStudentService;
    @Mock
    UpdateStudentDetailsService updateStudentDetailsService;
    @Mock
    ApplicationEventPublisher applicationEventPublisher;

    @InjectMocks
    FeeDueRecalculatorServiceImpl feeDueRecalculatorServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testJob() throws Exception {
        doNothing().when(applicationEventPublisher).publishEvent(any());
        when(accountTransactionRepository.getSumOfFeePaid(anyLong())).thenReturn(Optional.of(BigDecimal.ONE));
        when(feeDetailsFacade.getTotalFee(anyInt(), anyInt())).thenReturn(null);
        when(queryStudentFacade.findByEnrolmentNoAndValidateAdmissionStatus(anyLong())).thenReturn(null);
        final StudentNewAdmissionVM studentNewAdmissionVM = new StudentNewAdmissionVM(Long.valueOf(1), "name", new GregorianCalendar(2019, Calendar.MAY, 11, 9, 39).getTime(), new GregorianCalendar(2019, Calendar.MAY, 11, 9, 39).getTime(), new GregorianCalendar(2019, Calendar.MAY, 11, 9, 39).getTime(), "religion", "caste", "subcaste", Gender.MALE, "fatherName", "motherName", "occupation", "relation", "address1", "pin1", "address2", "pin2", "phone1", "phone2", "mobile", "email", "bloodGroup", "chechak", "nationality", "motherTongue", "areaOfInterest", Integer.valueOf(0), "lastSchoolName", Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), "board", Integer.valueOf(0), Integer.valueOf(0), "reasonForLeave", "transferCertificate", "lastSchoolMarkSheet", "conduct", Integer.valueOf(1), Integer.valueOf(1), Boolean.TRUE, Boolean.TRUE, "instrumentNo", AdmissionStatus.PENDING_FOR_APPROVAL);
        final StudentNewAdmissionEntity studentNewAdmissionEntity = new StudentAdmissionDetailMapperImpl().toEntity(studentNewAdmissionVM);

        when(updateStudentDetailsFacade.update(any())).thenReturn(studentNewAdmissionVM);
        when(queryFineRecordService.findByEnrolmentNo(anyLong())).thenReturn(Arrays.<FineRecordEntity>asList(new FineRecordEntity(1l, 1L, "fineType", new BigDecimal(0), "description", Status.ACTIVE)));
        when(accountMasterRepository.findAll()).thenReturn(EnhancedRandom.randomListOf(2, AccountMasterEntity.class));

        when(queryStudentService.findByEnrolmentNoAndValidateAdmissionStatus(any()))
                .thenReturn(Optional.of(studentNewAdmissionVM));

        when(studentNewAdmissionEntityRepository.findById(anyLong()))
                .thenReturn(Optional.of(studentNewAdmissionEntity));

        when(updateStudentDetailsService.update(studentNewAdmissionVM))
                .thenReturn(studentNewAdmissionVM);

        feeDueRecalculatorServiceImpl.job();
    }

    @Test
    public void testGetProcessName() throws Exception {
        String result = feeDueRecalculatorServiceImpl.getProcessName();
        Assert.assertEquals("FEE_RECALCULATOR", result);
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme