package org.vedanta.vidiyalay.account_service.services.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.stubbing.Answer;
import org.vedanta.vidiyalay.account_service.domain.FeeDetailsEntity;
import org.vedanta.vidiyalay.account_service.domain.mapper.FeeDetailsEntityMapper;
import org.vedanta.vidiyalay.account_service.domain.mapper.FeeDetailsEntityMapperImpl;
import org.vedanta.vidiyalay.account_service.repository.FeesDetailsRepository;
import org.vedanta.vidiyalay.account_service.web.rest.vm.FeeDetailsVM;
import org.vedanta.vidiyalay.utils.CloneObjects;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

public class QueryFeesServiceImplTest {
    @Mock
    FeesDetailsRepository feesDetailsRepository;
    @Mock
    FeeDetailsEntityMapper mapper;
    @InjectMocks
    QueryFeesServiceImpl queryFeesServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testFindAll() throws Exception {
        final FeeDetailsEntity feeDetailsEntity = new FeeDetailsEntity(1L,0, 0, "description", new BigDecimal(0), Boolean.TRUE);
        when(feesDetailsRepository.findAll()).thenReturn(Arrays.<FeeDetailsEntity>asList(feeDetailsEntity));
        final FeeDetailsVM dummy = new FeeDetailsEntityMapperImpl().toVM(feeDetailsEntity);
        final FeeDetailsVM feeDetailsVM = CloneObjects.clone(dummy, FeeDetailsVM.class, new CloneObjects.ParameterMap().put("id", 1L).build())
            .orElse(dummy);

//        final FeeDetailsVM feeDetailsVM = CloneObjects.clone(dummy, FeeDetailsVM.class, new CloneObjects.ParameterMap().put("id", 1L).build());
        Answer<FeeDetailsVM> answer = invocation -> feeDetailsVM;

        when(mapper.toVM(any())).then(answer);

        List<FeeDetailsVM> result = queryFeesServiceImpl.findAll();

        final List<FeeDetailsVM> expected = new ArrayList<>();
        expected.add(new FeeDetailsVM(Long.valueOf(1), 0, 0, "description", new BigDecimal(0), Boolean.TRUE));
        Assert.assertEquals(expected, result);
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme