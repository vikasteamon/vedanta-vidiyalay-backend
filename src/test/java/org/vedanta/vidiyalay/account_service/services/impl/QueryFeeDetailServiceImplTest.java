package org.vedanta.vidiyalay.account_service.services.impl;

import io.github.benas.randombeans.api.EnhancedRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.vedanta.vidiyalay.account_service.domain.AccountMasterEntity;
import org.vedanta.vidiyalay.account_service.domain.FeeDetailsEntity;
import org.vedanta.vidiyalay.account_service.domain.mapper.FeeDetailsEntityMapper;
import org.vedanta.vidiyalay.account_service.domain.mapper.FeeDetailsEntityMapperImpl;
import org.vedanta.vidiyalay.account_service.enums.AccountType;
import org.vedanta.vidiyalay.account_service.repository.AccountMasterRepository;
import org.vedanta.vidiyalay.account_service.repository.FeesDetailsRepository;
import org.vedanta.vidiyalay.account_service.services.AccountService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountMasterVM;
import org.vedanta.vidiyalay.account_service.web.rest.vm.FeeDetailsVM;
import org.vedanta.vidiyalay.student_service.domain.enums.AdmissionStatus;
import org.vedanta.vidiyalay.student_service.domain.enums.Gender;
import org.vedanta.vidiyalay.student_service.service.QueryStudentService;
import org.vedanta.vidiyalay.student_service.service.StudentNewAdmissionService;
import org.vedanta.vidiyalay.student_service.service.UpdateStudentDetailsService;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;

import java.math.BigDecimal;
import java.util.*;

import static org.mockito.Mockito.*;

public class QueryFeeDetailServiceImplTest {
    @Mock
    QueryStudentService queryStudentService;
    @Mock
    UpdateStudentDetailsService updateStudentDetailsService;
    @Mock
    StudentNewAdmissionService studentNewAdmissionService;
    @Mock
    AccountMasterRepository accountMasterRepository;
    @Mock
    FeesDetailsRepository feesDetailsRepository;
    @Mock
    FeeDetailsEntityMapper mapper;
    @Mock
    AccountService accountService;
    @InjectMocks
    QueryFeeDetailServiceImpl queryFeeDetailServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCalculateDueFee() throws Exception {
        final FeeDetailsVM feeDetailsVM = new FeeDetailsVM(Long.valueOf(1), 0, 0, "description", new BigDecimal(0), Boolean.TRUE);

        final StudentNewAdmissionVM studentNewAdmissionVM = new StudentNewAdmissionVM(Long.valueOf(1), "name", new GregorianCalendar(2019, Calendar.MAY, 15, 20, 14).getTime(), new GregorianCalendar(2019, Calendar.MAY, 15, 20, 14).getTime(), new GregorianCalendar(2019, Calendar.MAY, 15, 20, 14).getTime(), "religion", "caste", "subcaste", Gender.MALE, "fatherName", "motherName", "occupation", "relation", "address1", "pin1", "address2", "pin2", "phone1", "phone2", "mobile", "email", "bloodGroup", "chechak", "nationality", "motherTongue", "areaOfInterest", Integer.valueOf(0), "lastSchoolName", Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), "board", Integer.valueOf(0), Integer.valueOf(0), "reasonForLeave", "transferCertificate", "lastSchoolMarkSheet", "conduct", Integer.valueOf(0), Integer.valueOf(0), Boolean.TRUE, Boolean.TRUE, "instrumentNo", AdmissionStatus.PENDING_FOR_APPROVAL);

        when(queryStudentService.findByEnrolmentNoAndValidateAdmissionStatus(anyLong())).thenReturn(Optional.of(studentNewAdmissionVM));
        when(updateStudentDetailsService.update(any())).thenReturn(studentNewAdmissionVM);

        when(studentNewAdmissionService.findByParameters(null, feeDetailsVM.getStandard(), feeDetailsVM.getYear(),
                null, null, null, null))
                .thenReturn(EnhancedRandom.randomListOf(2, StudentNewAdmissionVM.class));

        when(accountService.updateAccount(any())).thenReturn(new AccountMasterVM(1L, "name", 1L, new GregorianCalendar(2019, Calendar.MAY, 15, 20, 14).getTime(), new BigDecimal(0), new BigDecimal(0), AccountType.STUDENT, new BigDecimal(0)));
        when(accountService.getAccountDetails(anyLong())).thenReturn(new AccountMasterVM(1L, "name", 1L, new GregorianCalendar(2019, Calendar.MAY, 15, 20, 14).getTime(), new BigDecimal(0), new BigDecimal(0), AccountType.STUDENT, new BigDecimal(0)));


        queryFeeDetailServiceImpl.calculateDueFee(feeDetailsVM);
    }

    @Test
    public void testGetStudentDetailsVM()  {
        final StudentNewAdmissionVM expected = EnhancedRandom.random(StudentNewAdmissionVM.class);
        when(queryStudentService.findByEnrolmentNoAndValidateAdmissionStatus(anyLong())).thenReturn(Optional.of(expected));


        StudentNewAdmissionVM result = queryFeeDetailServiceImpl.getStudentDetailsVM(Long.valueOf(1));
        Assert.assertEquals( expected, result);
    }

    @Test
    public void testGetTotalDueFee() throws Exception {
        when(accountMasterRepository.findByEnrolmentNo(anyLong())).thenReturn(new AccountMasterEntity(1L, "name", Long.valueOf(1), new GregorianCalendar(2019, Calendar.MAY, 15, 20, 14).getTime(), new BigDecimal(0), new BigDecimal(0), "accountType", new BigDecimal(0)));

        BigDecimal result = queryFeeDetailServiceImpl.getTotalDueFee(Long.valueOf(1));
        Assert.assertEquals(new BigDecimal(0), result);
    }

    @Test
    public void testGetFeeDetails()  {
        final FeeDetailsEntity feeDetailsEntity = new FeeDetailsEntity(1l, 0, 0, "description", BigDecimal.ZERO, Boolean.TRUE);
        when(feesDetailsRepository.findAllByStandardAndYear(anyInt(), anyInt())).thenReturn(Arrays.<FeeDetailsEntity>asList(feeDetailsEntity));
        final FeeDetailsEntityMapper feeDetailsEntityMapper = new FeeDetailsEntityMapperImpl();

        when(this.mapper.toVM(feeDetailsEntity))
                .then(e -> feeDetailsEntityMapper.toVM(feeDetailsEntity));

        List<FeeDetailsVM> result = queryFeeDetailServiceImpl.getFeeDetails(0, 0);
        Assert.assertEquals(Arrays.<FeeDetailsVM>asList(new FeeDetailsVM(Long.valueOf(1), 0, 0, "description", new BigDecimal(0), Boolean.TRUE)), result);
    }

    @Test
    public void testGetTotalFee()  {
        final FeeDetailsEntity feeDetailsEntity = new FeeDetailsEntity(1l, 0, 0, "description", new BigDecimal(0), Boolean.TRUE);
        when(feesDetailsRepository.findAllByStandardAndYear(anyInt(), anyInt())).thenReturn(Arrays.<FeeDetailsEntity>asList(feeDetailsEntity));
        when(mapper.toVM(feeDetailsEntity))
                .thenAnswer(e -> new FeeDetailsEntityMapperImpl().toVM(feeDetailsEntity));
        Optional<BigDecimal> result = queryFeeDetailServiceImpl.getTotalFee(0, 0);
        Assert.assertEquals(BigDecimal.ZERO, result.orElse(BigDecimal.ZERO));
    }

}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme

