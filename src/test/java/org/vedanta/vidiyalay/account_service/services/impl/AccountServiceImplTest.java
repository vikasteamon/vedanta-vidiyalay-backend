package org.vedanta.vidiyalay.account_service.services.impl;

import io.github.benas.randombeans.api.EnhancedRandom;
import org.assertj.core.api.Assertions;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.vedanta.vidiyalay.account_service.domain.AccountMasterEntity;
import org.vedanta.vidiyalay.account_service.domain.mapper.AccountEntityMapper;
import org.vedanta.vidiyalay.account_service.domain.mapper.AccountEntityMapperImpl;
import org.vedanta.vidiyalay.account_service.facade.impl.ExceptionMailer;
import org.vedanta.vidiyalay.account_service.repository.AccountMasterRepository;
import org.vedanta.vidiyalay.account_service.services.QueryFeeDetailService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountMasterVM;
import org.vedanta.vidiyalay.email_service.SendEmailNotification;
import org.vedanta.vidiyalay.student_service.domain.enums.AdmissionStatus;
import org.vedanta.vidiyalay.student_service.domain.enums.Gender;
import org.vedanta.vidiyalay.student_service.service.QueryStudentService;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Optional;

import static org.mockito.Mockito.*;

public class AccountServiceImplTest {
    @Mock
    AccountMasterRepository accountMasterRepository;
    @Mock
    AccountEntityMapper accountEntityMapper;
    @Mock
    QueryFeeDetailService queryFeeDetailService;
    @Mock
    ExceptionMailer exceptionMailer;
    @Mock
    SendEmailNotification sendEmailNotification;
    @Mock
    QueryStudentService queryStudentService;
    @Mock
    Logger log;
    @InjectMocks
    AccountServiceImpl accountServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCreateNewAccount() throws Exception {
        final AccountMasterEntity accountMasterEntity = new AccountMasterEntity(1L, "name", Long.valueOf(1), new GregorianCalendar(2019, Calendar.MAY, 10, 10, 25).getTime(), new BigDecimal(0), new BigDecimal(0), "STUDENT", new BigDecimal(0));
        final AccountEntityMapperImpl accountEntityMapper = new AccountEntityMapperImpl();

        when(accountMasterRepository.findByEnrolmentNo(1L)).thenReturn(null);
        when(accountMasterRepository.save(any(AccountMasterEntity.class))).thenReturn(accountMasterEntity);
        when(this.accountEntityMapper.toEntity(any())).thenReturn(accountMasterEntity);
        final AccountMasterVM accountMasterVM = accountEntityMapper.toVm(accountMasterEntity);
        when(this.accountEntityMapper.toVm(any())).thenReturn(accountMasterVM);
        when(queryFeeDetailService.getTotalFee(anyInt(), anyInt())).thenReturn(Optional.of(BigDecimal.ONE));
        final StudentNewAdmissionVM studentNewAdmissionVM = EnhancedRandom.random(StudentNewAdmissionVM.class);
        when(queryStudentService.findByEnrolmentNoAndValidateAdmissionStatus(anyLong())).thenReturn(
                Optional.of(studentNewAdmissionVM)
        );
        AccountMasterVM result = accountServiceImpl.createNewAccount(studentNewAdmissionVM, BigDecimal.ONE);

        Assertions.assertThat(result).isEqualTo(accountMasterVM);

    }

    @Test
    public void testUpdateAccount() throws Exception {
        final AccountEntityMapperImpl accountEntityMapper = new AccountEntityMapperImpl();
        final AccountMasterEntity accountMasterEntity = new AccountMasterEntity(1l, "name", Long.valueOf(1), new GregorianCalendar(2019, Calendar.MAY, 10, 15, 54).getTime(), new BigDecimal(0), new BigDecimal(0), "STUDENT", new BigDecimal(0));
        final AccountMasterVM accountMasterVM = accountEntityMapper.toVm(accountMasterEntity);
        when(this.accountEntityMapper.toEntity(accountMasterVM)).thenReturn(accountMasterEntity);
        when(this.accountEntityMapper.toVm(any())).thenReturn(accountMasterVM);
        when(accountMasterRepository.save(accountMasterEntity)).thenReturn(accountMasterEntity);

        AccountMasterVM result = accountServiceImpl.updateAccount(accountMasterVM);
        Assert.assertEquals(accountMasterVM, result);
    }

    @Test
    public void testGetAccountDetails() throws Exception {
        final AccountMasterEntity accountMasterEntity = new AccountMasterEntity(1L, "name", Long.valueOf(1), new GregorianCalendar(2019, Calendar.MAY, 10, 15, 54).getTime(), new BigDecimal(0), new BigDecimal(0), "STUDENT", new BigDecimal(0));
        when(accountMasterRepository.findByEnrolmentNo(anyLong())).thenReturn(accountMasterEntity);
        final AccountEntityMapperImpl accountEntityMapper = new AccountEntityMapperImpl();
        final AccountMasterVM accountMasterVM = accountEntityMapper.toVm(accountMasterEntity);
        when(this.accountEntityMapper.toVm(any())).thenReturn(accountMasterVM);

        AccountMasterVM result = accountServiceImpl.getAccountDetails(Long.valueOf(1));

        verify(accountMasterRepository).findByEnrolmentNo(anyLong());

    }

    @Test
    public void testCreateAccountListener() throws Exception {
        final AccountMasterEntity accountMasterEntity = new AccountMasterEntity(1L, "name", Long.valueOf(1), new GregorianCalendar(2019, Calendar.MAY, 10, 15, 54).getTime(), new BigDecimal(0), new BigDecimal(0), "STUDENT", new BigDecimal(0));

        final AccountEntityMapperImpl accountEntityMapper = new AccountEntityMapperImpl();
        final AccountMasterVM accountMasterVM = accountEntityMapper.toVm(accountMasterEntity);

        when(accountMasterRepository.findByEnrolmentNo(anyLong())).thenReturn(null);
        when(this.accountEntityMapper.toEntity(any(AccountMasterVM.class))).thenReturn(accountMasterEntity);
        when(this.accountEntityMapper.toVm(any(AccountMasterEntity.class))).thenReturn(accountMasterVM);
        when(queryFeeDetailService.getTotalFee(anyInt(), anyInt())).thenReturn(Optional.of(BigDecimal.ONE));
        when(accountMasterRepository.save(accountMasterEntity)).thenReturn(accountMasterEntity);

        final StudentNewAdmissionVM studentNewAdmissionVM = new StudentNewAdmissionVM(Long.valueOf(1), "name", new GregorianCalendar(2019, Calendar.MAY, 10, 15, 54).getTime(), new GregorianCalendar(2019, Calendar.MAY, 10, 15, 54).getTime(), new GregorianCalendar(2019, Calendar.MAY, 10, 15, 54).getTime(), "religion", "caste", "subcaste", Gender.MALE, "fatherName", "motherName", "occupation", "relation", "address1", "pin1", "address2", "pin2", "phone1", "phone2", "mobile", "email", "bloodGroup", "chechak", "nationality", "motherTongue", "areaOfInterest", Integer.valueOf(0), "lastSchoolName", Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), "board", Integer.valueOf(0), Integer.valueOf(0), "reasonForLeave", "transferCertificate", "lastSchoolMarkSheet", "conduct", Integer.valueOf(0), Integer.valueOf(0), Boolean.TRUE, Boolean.TRUE, "instrumentNo", AdmissionStatus.PENDING_FOR_APPROVAL);
        accountServiceImpl.createAccountListener(studentNewAdmissionVM, BigDecimal.ONE);
    }
//
//    @Test
//    public void testReceiveMessage_fallback() throws Exception {
//        when(accountMasterRepository.findByEnrolmentNo(anyLong())).thenReturn(new AccountMasterEntity("name", Long.valueOf(1), new GregorianCalendar(2019, Calendar.MAY, 10, 15, 54).getTime(), new BigDecimal(0), new BigDecimal(0), "STUDENT", new BigDecimal(0)));
//        when(accountEntityMapper.toEntity(any())).thenReturn(new AccountMasterEntity("name", Long.valueOf(1), new GregorianCalendar(2019, Calendar.MAY, 10, 15, 54).getTime(), new BigDecimal(0), new BigDecimal(0), "STUDENT", new BigDecimal(0)));
//        when(queryFeeDetailService.getTotalFee(anyInt(), anyInt())).thenReturn(null);
//
//        accountServiceImpl.receiveMessage_fallback(new StudentNewAdmissionVM(Long.valueOf(1), "name", new GregorianCalendar(2019, Calendar.MAY, 10, 15, 54).getTime(), new GregorianCalendar(2019, Calendar.MAY, 10, 15, 54).getTime(), new GregorianCalendar(2019, Calendar.MAY, 10, 15, 54).getTime(), "religion", "caste", "subcaste", Gender.MALE, "fatherName", "motherName", "occupation", "relation", "address1", "pin1", "address2", "pin2", "phone1", "phone2", "mobile", "email", "bloodGroup", "chechak", "nationality", "motherTongue", "areaOfInterest", Integer.valueOf(0), "lastSchoolName", Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), "board", Integer.valueOf(0), Integer.valueOf(0), "reasonForLeave", "transferCertificate", "lastSchoolMarkSheet", "conduct", Integer.valueOf(0), Integer.valueOf(0), Boolean.TRUE, Boolean.TRUE, "instrumentNo", AdmissionStatus.PENDING_FOR_APPROVAL, null));
//    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme