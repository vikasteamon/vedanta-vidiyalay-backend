package org.vedanta.vidiyalay.account_service.services.impl;

import io.github.benas.randombeans.api.EnhancedRandom;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.vedanta.vidiyalay.account_service.repository.AccountTransactionRepository;
import org.vedanta.vidiyalay.account_service.services.UpdateFeeDetailService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountTransactionVo;
import org.vedanta.vidiyalay.email_service.SendEmailNotification;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;

import java.math.BigDecimal;

import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;

public class FeeDepositServiceImplTest {
    @Mock
    private AccountTransactionRepository accountTransactionRepository;
    @Mock
    private SendEmailNotification sendEmailNotification;
    @Mock
    private UpdateFeeDetailService updateFeeDetailService;
    @InjectMocks
    private FeeDepositServiceImpl feeDepositServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testDepositFee() throws Exception {
        when(updateFeeDetailService.calculateAndUpdateFeeDueStatus(any(), any())).thenReturn(new BigDecimal(0));
        final StudentNewAdmissionVM studentNewAdmissionVM = EnhancedRandom.random(StudentNewAdmissionVM.class);
        BigDecimal result = feeDepositServiceImpl.depositFee(new AccountTransactionVo(Long.valueOf(1), new BigDecimal(0), "instrumentNo", "transactionMode"),
                studentNewAdmissionVM, BigDecimal.ONE);
        Assert.assertEquals(new BigDecimal(0), result);
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme