package org.vedanta.vidiyalay.account_service.services.impl;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.vedanta.vidiyalay.account_service.domain.FeeDetailsEntity;
import org.vedanta.vidiyalay.account_service.domain.mapper.FeeDetailsEntityMapper;
import org.vedanta.vidiyalay.account_service.enums.AccountType;
import org.vedanta.vidiyalay.account_service.repository.FeesDetailsRepository;
import org.vedanta.vidiyalay.account_service.services.AccountService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountMasterVM;
import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountTransactionVo;
import org.vedanta.vidiyalay.account_service.web.rest.vm.FeeDetailsVM;
import org.vedanta.vidiyalay.student_service.domain.StudentNewAdmissionEntity;
import org.vedanta.vidiyalay.student_service.domain.enums.AdmissionStatus;
import org.vedanta.vidiyalay.student_service.domain.enums.Gender;
import org.vedanta.vidiyalay.student_service.mapper.StudentAdmissionDetailMapperImpl;
import org.vedanta.vidiyalay.student_service.repository.StudentNewAdmissionEntityRepository;
import org.vedanta.vidiyalay.student_service.service.QueryStudentService;
import org.vedanta.vidiyalay.student_service.service.UpdateStudentDetailsService;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Optional;

import static org.mockito.Mockito.*;

public class UpdateFeeDetailServiceImplTest {
    @Mock
    QueryStudentService queryStudentService;
    @Mock
    UpdateStudentDetailsService updateStudentDetailsService;
    @Mock
    FeesDetailsRepository feesDetailsRepository;
    @Mock
    FeeDetailsEntityMapper mapper;
    @Mock
    AccountService accountService;
    @InjectMocks
    UpdateFeeDetailServiceImpl updateFeeDetailServiceImpl;
    @Mock
    StudentNewAdmissionEntityRepository studentNewAdmissionEntityRepository;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testCalculateAndUpdateFeeDueStatus() throws Exception {

        final StudentNewAdmissionVM studentNewAdmissionVM = new StudentNewAdmissionVM(Long.valueOf(1), "name", new GregorianCalendar(2019, Calendar.MAY, 15, 22, 16).getTime(), new GregorianCalendar(2019, Calendar.MAY, 15, 22, 16).getTime(), new GregorianCalendar(2019, Calendar.MAY, 15, 22, 16).getTime(), "religion", "caste", "subcaste", Gender.MALE, "fatherName", "motherName", "occupation", "relation", "address1", "pin1", "address2", "pin2", "phone1", "phone2", "mobile", "email", "bloodGroup", "chechak", "nationality", "motherTongue", "areaOfInterest", Integer.valueOf(0), "lastSchoolName", Integer.valueOf(0), Integer.valueOf(0), Integer.valueOf(0), "board", Integer.valueOf(0), Integer.valueOf(0), "reasonForLeave", "transferCertificate", "lastSchoolMarkSheet", "conduct", Integer.valueOf(0), Integer.valueOf(0), Boolean.TRUE, Boolean.TRUE, "instrumentNo", AdmissionStatus.PENDING_FOR_APPROVAL);
        when(queryStudentService.findByEnrolmentNoAndValidateAdmissionStatus(anyLong())).thenReturn(Optional.of(studentNewAdmissionVM));
        final StudentAdmissionDetailMapperImpl studentAdmissionDetailMapper = new StudentAdmissionDetailMapperImpl();
        final StudentNewAdmissionEntity studentNewAdmissionEntity = studentAdmissionDetailMapper.toEntity(studentNewAdmissionVM);

        when(updateStudentDetailsService.update(any())).thenReturn(studentNewAdmissionVM);
        when(accountService.updateAccount(any())).thenReturn(new AccountMasterVM(1L, "name", Long.valueOf(1), new GregorianCalendar(2019, Calendar.MAY, 15, 22, 16).getTime(), new BigDecimal(0), new BigDecimal(0), AccountType.STUDENT, new BigDecimal(0)));
        when(accountService.getAccountDetails(anyLong())).thenReturn(new AccountMasterVM(1L, "name", Long.valueOf(1), new GregorianCalendar(2019, Calendar.MAY, 15, 22, 16).getTime(), new BigDecimal(0), new BigDecimal(0), AccountType.STUDENT, new BigDecimal(0)));
        when(studentNewAdmissionEntityRepository.findById(anyLong()))
                .thenReturn(Optional.of(studentNewAdmissionEntity));

        BigDecimal result = updateFeeDetailServiceImpl.calculateAndUpdateFeeDueStatus(new AccountTransactionVo(Long.valueOf(1), new BigDecimal(0), "instrumentNo", "transactionMode"), new BigDecimal(0));
        Assert.assertEquals(new BigDecimal(0), result);
    }

    @Test
    public void testCreateFeeDetail() throws Exception {
        when(mapper.toEntity(any())).thenReturn(new FeeDetailsEntity(1l, 0, 0, "description", new BigDecimal(0), Boolean.TRUE));
        when(mapper.toVM(any())).thenReturn(new FeeDetailsVM(Long.valueOf(1), 0, 0, "description", new BigDecimal(0), Boolean.TRUE));

        FeeDetailsVM result = updateFeeDetailServiceImpl.createFeeDetail(new FeeDetailsVM(Long.valueOf(1), 0, 0, "description", new BigDecimal(0), Boolean.TRUE));
        Assert.assertEquals(new FeeDetailsVM(Long.valueOf(1), 0, 0, "description", new BigDecimal(0), Boolean.TRUE), result);
    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme