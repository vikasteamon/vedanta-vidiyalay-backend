package org.vedanta.vidiyalay.utility_service.services.impl;

import io.github.benas.randombeans.api.EnhancedRandom;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.vedanta.vidiyalay.account_service.enums.AccountType;
import org.vedanta.vidiyalay.account_service.events.CustomApplicationEvents;
import org.vedanta.vidiyalay.account_service.events.EventType;
import org.vedanta.vidiyalay.account_service.services.AccountService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountMasterVM;
import org.vedanta.vidiyalay.config.ApplicationProperties;
import org.vedanta.vidiyalay.email_service.SendEmailNotification;
import org.vedanta.vidiyalay.fine_service.service.QueryFineRecordService;
import org.vedanta.vidiyalay.student_service.domain.StudentNewAdmissionEntity;
import org.vedanta.vidiyalay.student_service.domain.enums.AdmissionStatus;
import org.vedanta.vidiyalay.student_service.service.StudentNewAdmissionService;
import org.vedanta.vidiyalay.student_service.service.UpdateStudentDetailsService;

import java.math.BigDecimal;
import java.util.*;
import java.util.stream.IntStream;

import static org.mockito.Mockito.*;

public class TerminateStudentServiceImplTest {
    @Mock
    AccountService accountService;
    @Mock
    StudentNewAdmissionService studentNewAdmissionService;
    @Mock
    QueryFineRecordService queryFineRecordService;
    @Mock
    UpdateStudentDetailsService updateStudentDetailsService;
    @Mock
    SendEmailNotification sendEmailNotification;
    @Mock
    ApplicationProperties applicationProperties;
    @Mock
    Logger log;
    @InjectMocks
    TerminateStudentServiceImpl terminateStudentServiceImpl;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testTerminateStudentScheduler() throws Exception {
        when(accountService.getAccountDetailsNoError(anyLong())).thenReturn(new AccountMasterVM(1L, "name", 1L, new GregorianCalendar(2019, Calendar.MAY, 25, 17, 25).getTime(), new BigDecimal(0), new BigDecimal(0), AccountType.STUDENT, new BigDecimal(0)));
        final List<StudentNewAdmissionEntity> studentNewAdmissionEntities = new ArrayList<>(20);

        AdmissionStatus[] admissionStatuses
                = new AdmissionStatus[]{AdmissionStatus.TERMINATION_INITIATED, AdmissionStatus.PENDING_FOR_APPROVAL,
                AdmissionStatus.APPROVED, AdmissionStatus.PENDING_FOR_APPROVAL, AdmissionStatus.SUSPENDED};

        final ApplicationProperties.Notifications notifications = new ApplicationProperties.Notifications();
        final HashMap<String, String> mailAddress = new HashMap<>();
        mailAddress.put("admin", "vikas.on@gmail.com");
        notifications.setMailAddress(mailAddress);

        when(applicationProperties.getNotifications()).thenReturn(notifications);

        IntStream.range(1, 20)
                .forEach(e -> {
                    StudentNewAdmissionEntity random = EnhancedRandom.random(StudentNewAdmissionEntity.class);
                    random.setIsFeeDue(e % 2 == 0);
                    random.setAdmissionStatus(admissionStatuses[new Random().nextInt(admissionStatuses.length)]);
                    studentNewAdmissionEntities.add(random);
                });

        final int numberOfStudentWithDueFee
                = Math.toIntExact(studentNewAdmissionEntities
                .stream().filter(StudentNewAdmissionEntity::getIsFeeDue)
                .filter(e -> e.getAdmissionStatus() == AdmissionStatus.TERMINATION_INITIATED).count());

        when(studentNewAdmissionService.findAll()).thenReturn(studentNewAdmissionEntities);
        doNothing().when(sendEmailNotification).sendEmail(any());
        terminateStudentServiceImpl.terminateStudentScheduler(
                new CustomApplicationEvents("source", EventType.PROCESS_TERMINATION_IN_PROGRESS_STUDENTS));
//        verify(accountService, atMost(numberOfStudentWithDueFee)).getAccountDetailsNoError(anyLong());
        verify(studentNewAdmissionService).findAll();
    }

    @Test
    public void testTerminateStudentThatTerminationInitiated() throws Exception {
        final Iterable<StudentNewAdmissionEntity> studentNewAdmissionEntities
                = EnhancedRandom.randomListOf(5, StudentNewAdmissionEntity.class);

        final AccountMasterVM accountMasterVM = new AccountMasterVM(1L, "accountMasterVM", 1L, new GregorianCalendar(2019, Calendar.MAY, 25, 17, 25).getTime(), new BigDecimal(0), new BigDecimal(0), AccountType.STUDENT, new BigDecimal(0));
        when(accountService.getAccountDetailsNoError(anyLong())).thenReturn(accountMasterVM);

        when(studentNewAdmissionService.findAll()).thenReturn(studentNewAdmissionEntities);
        doNothing().when(sendEmailNotification).sendEmail(any());
        final ApplicationProperties.Notifications notifications = new ApplicationProperties.Notifications();
        final HashMap<String, String> mailAddress = new HashMap<>();
        mailAddress.put("admin", "vikas.on@gmail.com");
        notifications.setMailAddress(mailAddress);

        when(applicationProperties.getNotifications()).thenReturn(notifications);
        terminateStudentServiceImpl.terminateStudentThatTerminationInitiated(accountService,
                studentNewAdmissionService, queryFineRecordService, updateStudentDetailsService);
    }
}

