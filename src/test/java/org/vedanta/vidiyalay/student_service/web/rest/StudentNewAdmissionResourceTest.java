/*
 *     Copyright (C) 2019  Vikas Kumar Verma
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.vedanta.vidiyalay.student_service.web.rest;


import io.github.benas.randombeans.api.EnhancedRandom;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.vedanta.vidiyalay.student_service.Endpoints;
import org.vedanta.vidiyalay.student_service.facade.CreateUpdateStudentFacade;
import org.vedanta.vidiyalay.student_service.facade.UpdateStudentDetailsFacade;
import org.vedanta.vidiyalay.student_service.web.rest.vm.AdmissionStatusVM;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;

import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = StudentNewAdmissionResourceTestMockConfig.class)
@WebMvcTest(secure = false, value = StudentNewAdmissionResource.class)
public class StudentNewAdmissionResourceTest {

    private MockMvc mvc;

    @Mock
    private CreateUpdateStudentFacade createUpdateStudentFacade;

    @Mock
    private UpdateStudentDetailsFacade updateStudentDetailsFacade;

    private  static final StudentNewAdmissionVM studentNewAdmissionVM = EnhancedRandom.random(StudentNewAdmissionVM.class, "classDetailsVM" );

    @Before
    public void setup() {
        this.mvc = MockMvcBuilders.standaloneSetup(new StudentNewAdmissionResource(createUpdateStudentFacade,
                updateStudentDetailsFacade)).build();
    }


    @Test
    public void newAdmission() throws Exception {
        //given
        Mockito.when(createUpdateStudentFacade.newAdmission(any(StudentNewAdmissionVM.class)))
                .thenReturn(studentNewAdmissionVM);

        //when
        this.mvc.perform(post(Endpoints.studentResource+Endpoints.newAdmissionPost)
                .content(new ObjectMapper().writeValueAsString(studentNewAdmissionVM).getBytes())
                .contentType(MediaType.APPLICATION_JSON)
                .characterEncoding("UTF8")
                .accept(MediaType.APPLICATION_JSON))

                .andExpect(status().isOk());

    }

    @Test
    public void updateAdmissionStatus() throws Exception {
        //given
        final AdmissionStatusVM request = EnhancedRandom.random(AdmissionStatusVM.class );

        Mockito.when(updateStudentDetailsFacade.update(any(StudentNewAdmissionVM.class)))
                .thenReturn(studentNewAdmissionVM);

        //when
        this.mvc.perform(post(Endpoints.studentResource+Endpoints.updateStudentPost)
                .content(new ObjectMapper().writeValueAsString(request))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}

@Configuration
class StudentNewAdmissionResourceTestMockConfig {

    @InjectMocks
    CreateUpdateStudentFacade facade;

}
