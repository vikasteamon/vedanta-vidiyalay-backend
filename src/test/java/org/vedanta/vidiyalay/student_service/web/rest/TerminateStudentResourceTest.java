/*
 *     Copyright (C) 2019  Vikas Kumar Verma
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.vedanta.vidiyalay.student_service.web.rest;

import io.github.benas.randombeans.api.EnhancedRandom;
import lombok.Getter;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.vedanta.vidiyalay.student_service.Endpoints;
import org.vedanta.vidiyalay.student_service.facade.CreateUpdateStudentFacade;
import org.vedanta.vidiyalay.student_service.facade.UpdateStudentDetailsFacade;
import org.vedanta.vidiyalay.student_service.web.rest.vm.AdmissionStatusVM;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;
import org.vedanta.vidiyalay.student_service.web.rest.vm.TerminateStudentVM;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = StudentNewAdmissionResourceTestMockConfig.class)
@WebMvcTest(secure = false, value = StudentNewAdmissionResource.class)
public class TerminateStudentResourceTest {

    private MockMvc mvc;

    @Mock
    private CreateUpdateStudentFacade createUpdateStudentFacade;

    @Mock
    private UpdateStudentDetailsFacade updateStudentDetailsFacade;


    @Before
    public void setup() {
        this.mvc = MockMvcBuilders.standaloneSetup(new TerminateStudentResource(updateStudentDetailsFacade)).build();
    }

    @Test
    public void terminated() throws Exception {
        final TerminateStudentVM requestVM = EnhancedRandom.random(TerminateStudentVM.class );
        final StudentNewAdmissionVM response = EnhancedRandom.random(StudentNewAdmissionVM.class);
        //given
        final AdmissionStatusVM request = EnhancedRandom.random(AdmissionStatusVM.class );

        Mockito.when(updateStudentDetailsFacade.terminate(requestVM))
                .thenReturn(response);


        //when
        this.mvc.perform(post(Endpoints.terminateStudentResource)
                .content(ObjectMapperTest.MAPPER.getMapper().writeValueAsString(requestVM))
                .contentType(MediaType.APPLICATION_JSON_UTF8_VALUE)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }
}

@Getter
enum ObjectMapperTest {
    MAPPER;

    public ObjectMapper mapper = new ObjectMapper();


}
