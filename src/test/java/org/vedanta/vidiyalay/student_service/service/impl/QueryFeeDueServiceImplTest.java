/*
 *     Copyright (C) 2019  Vikas Kumar Verma
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.vedanta.vidiyalay.student_service.service.impl;

import io.github.benas.randombeans.api.EnhancedRandom;
import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.vedanta.vidiyalay.student_service.domain.enums.AdmissionStatus;
import org.vedanta.vidiyalay.student_service.repository.QueryFeeDueRepository;
import org.vedanta.vidiyalay.student_service.web.rest.vm.DueFeesDetailsVM;

import java.math.BigDecimal;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)

public class QueryFeeDueServiceImplTest {


    @Mock
    private QueryFeeDueRepository queryFeeDueRepository;

    @InjectMocks
    private QueryFeeDueServiceImpl queryFeeDueService;

    @Test
    public void findDueFee() {
        final Long enrolmentNo = 1L;
        final Integer standard = 1;
        final String name = "dummy";
        final AdmissionStatus admissionStatus = AdmissionStatus.APPROVED;
        final BigDecimal amount = BigDecimal.ONE;
        final String fatherName = "dummy father";

        final List<DueFeesDetailsVM> mockResponse = EnhancedRandom.randomListOf(10, DueFeesDetailsVM.class);
        //mock
        Mockito.when(queryFeeDueRepository.findDueFee(enrolmentNo, standard, name, admissionStatus, amount, fatherName))
                .thenReturn(mockResponse);

        //when
        final List<DueFeesDetailsVM> result = queryFeeDueService.findDueFee(enrolmentNo, standard, name, admissionStatus, amount, fatherName);

        //verify
        Mockito.verify(queryFeeDueRepository).findDueFee(enrolmentNo, standard, name, admissionStatus, amount, fatherName);
        Assertions.assertThat(result).isNotIn(mockResponse);
    }
}