/*
 *     Copyright (C) 2019  Vikas Kumar Verma
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.vedanta.vidiyalay.reports;

import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRSaver;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

@RestController
@Slf4j
public class ReportResource {

    private static final String SCHOLER_REPORT_JRXML = "jasper/sample.jrxml";
    private static final String SCHOLER_REPORT_JASPER = "jasper/sample.jasper";
    private final DataSource dataSource;

    ReportResource(DataSource dataSource) {
        this.dataSource = dataSource;
    }


    @GetMapping(value = "/enrolment/{enrolmentNo}/scholer_report.pdf", produces = MediaType.APPLICATION_PDF_VALUE)
    @ResponseBody
    @CrossOrigin(origins = "http://localhost:4200")
    HttpEntity<byte[]> printScholerReport(@PathVariable(name ="enrolmentNo") final long enrolmentNo)
        throws JRException, SQLException, FileNotFoundException {

        Assert.notNull(dataSource, "data source is null");

        // load jasper report
        JasperReport jasperReport = getJasperReport();


        // set report parameters
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("p_eno", enrolmentNo);

        // get the pdf reports in bytes
        byte[] bytes = getReportsBytes(jasperReport, parameters);

        //create response header
        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_PDF);
        header.set(HttpHeaders.CONTENT_DISPOSITION, "inline; filename=scholer_report.pdf");
        header.setContentLength(bytes.length);

        return new HttpEntity<>(bytes, header);

    }

    private byte[] getReportsBytes(JasperReport jasperReport, Map<String, Object> parameters) throws JRException, SQLException {
        Connection connection = dataSource.getConnection();
        JasperPrint jasperPrint = JasperFillManager.fillReport(
            jasperReport, parameters, connection);
        connection.close();

        return JasperExportManager.exportReportToPdf(jasperPrint);
    }

    private JasperReport getJasperReport() throws JRException, FileNotFoundException {
        File file = new File(SCHOLER_REPORT_JRXML);
        Assert.isTrue( file.exists(), "file not found");

        JasperReport jasperReport
            = JasperCompileManager.compileReport(new FileInputStream(file));

        JRSaver.saveObject(jasperReport, SCHOLER_REPORT_JASPER);
        return jasperReport;
    }
}
