/*
 *     Copyright (C) 2019  Vikas Kumar Verma
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.vedanta.vidiyalay.student_service.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.vedanta.vidiyalay.account_service.events.CustomApplicationEvents;
import org.vedanta.vidiyalay.account_service.events.EventType;
import org.vedanta.vidiyalay.config.kafka.StudentDetailsStreams;
import org.vedanta.vidiyalay.student_service.domain.StudentNewAdmissionEntity;
import org.vedanta.vidiyalay.student_service.domain.enums.AdmissionStatus;
import org.vedanta.vidiyalay.student_service.mapper.StudentAdmissionDetailMapper;
import org.vedanta.vidiyalay.student_service.service.CreateUpdateStudentService;
import org.vedanta.vidiyalay.student_service.service.StudentNewAdmissionService;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;
import org.vedanta.vidiyalay.utils.Utility;

import java.util.Optional;

//import org.springframework.messaging.MessageHeaders;
//import org.springframework.messaging.support.MessageBuilder;

@Slf4j
@Component
public class CreateUpdateStudentServiceImpl implements CreateUpdateStudentService {


    private final StudentNewAdmissionService studentService;
    private final StudentAdmissionDetailMapper mapper;
    private final StudentDetailsStreams studentDetailsStreams;
    private final ApplicationEventPublisher applicationEventPublisher;

    public CreateUpdateStudentServiceImpl(StudentNewAdmissionService studentService, StudentAdmissionDetailMapper mapper, StudentDetailsStreams studentDetailsStreams, ApplicationEventPublisher applicationEventPublisher) {
        this.studentService = studentService;
        this.mapper = mapper;
        this.studentDetailsStreams = studentDetailsStreams;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    @Transactional
    public StudentNewAdmissionVM newAdmission(final StudentNewAdmissionVM s) {
        Assert.notNull(studentDetailsStreams, "studentDetailsStreams IS NULL!");

        final StudentNewAdmissionEntity studentNewAdmissionEntity = mapper.toEntity(s);

        studentNewAdmissionEntity.setIsFeeDue(true);
        studentNewAdmissionEntity.setIsWithdrawal(false);
        studentNewAdmissionEntity.setAdmissionStatus(AdmissionStatus.PENDING_FOR_APPROVAL);

        studentNewAdmissionEntity.setDateOfAdmission(Utility.getCurrentDateTime());

        final StudentNewAdmissionVM studentNewAdmissionVM = Optional
                .ofNullable(studentService.newAdmission(studentNewAdmissionEntity))
                .map(mapper::toVm)
                .orElseThrow(() -> new RuntimeException("System cannot press request now."));

        //create event about new student created
        applicationEventPublisher.publishEvent(
                new CustomApplicationEvents(studentNewAdmissionVM, EventType.STUDENT_CREATED)
        );



        return studentNewAdmissionVM;
    }


}
