package org.vedanta.vidiyalay.student_service.service;

import org.springframework.transaction.annotation.Transactional;
import org.vedanta.vidiyalay.student_service.domain.StudentNewAdmissionEntity;
import org.vedanta.vidiyalay.student_service.domain.enums.AdmissionStatus;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;

import java.util.List;
import java.util.Optional;

public interface StudentNewAdmissionService {
    StudentNewAdmissionEntity newAdmission(StudentNewAdmissionEntity studentNewAdmissionEntity);

    StudentNewAdmissionEntity updateStudentDetails(StudentNewAdmissionEntity studentNewAdmissionEntity);

    Optional<StudentNewAdmissionEntity> findByEnrolmentNo(Long id);

    Iterable<StudentNewAdmissionEntity> findAll();

    @Transactional(readOnly = true)
    List<StudentNewAdmissionVM> findByParameters(Long enrolmentNo, Integer standard, Integer year, String name, AdmissionStatus admissionStatus, String fatherName, String motherName);
}
