/*
 *     Copyright (C) 2019  Vikas Kumar Verma
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.vedanta.vidiyalay.email_service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.vedanta.vidiyalay.account_service.events.CustomApplicationEvents;
import org.vedanta.vidiyalay.account_service.events.EventType;
import org.vedanta.vidiyalay.email_service.web.rest.vm.EmailVM;

import java.util.Collections;
import java.util.Map;

@Component
@Slf4j
public class SendEmailNotification {

    private final JavaMailSender javaMailSender;
    private final TemplateEngine templateEngine;
    private final ApplicationEventPublisher applicationEventPublisher;

    @Value("${jhipster.mail.enabled}")
    private boolean enabled;

    @Value("${jhipster.mail.from}")
    private String DEFAULT_EMAIL_FROM;

    public SendEmailNotification(JavaMailSender javaMailSender, TemplateEngine templateEngine, ApplicationEventPublisher applicationEventPublisher) {
        this.javaMailSender = javaMailSender;
        this.templateEngine = templateEngine;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    public void sendEmail(final EmailVM emailVM) {
        log.debug("sending mail: {}", emailVM);
        applicationEventPublisher.publishEvent(new CustomApplicationEvents(emailVM, EventType.SENDING_EMAIL));
    }

//    @StreamListener(EmailStreamConfig.INPUT)
    @EventListener(CustomApplicationEvents.class)
    @Async
    public void sendEventEventListener(final CustomApplicationEvents customApplicationEvents) {
        final EmailVM emailVM = customApplicationEvents.getSourceFromEvent(EventType.SENDING_EMAIL, EmailVM.class);
        if (null == emailVM) {
            return;
        }
        sendEmailStreamListener(emailVM);
    }
    public void sendEmailStreamListener( final EmailVM emailVM) {

        if (StringUtils.isEmpty(emailVM.getTo())
                && ObjectUtils.isEmpty(emailVM.getTos())) {
            log.debug("To address is null");
            return;
        }

        final MimeMessagePreparator messagePreparation = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setFrom((StringUtils.isEmpty(emailVM.getFrom()) ?
                    DEFAULT_EMAIL_FROM : emailVM.getFrom()));
            messageHelper.setTo((ObjectUtils.isEmpty(emailVM.getTos())) ? (String[]) Collections.singletonList(emailVM.getTo()).toArray() : emailVM.getTos());
            messageHelper.setSubject(emailVM.getSubject());
            messageHelper.setText(createMailTemplate(emailVM.getTemplateFile(), emailVM.getParams()), true);
        };

        log.info("sending mail: {}", messagePreparation.toString());

        if (enabled) {
            javaMailSender.send(messagePreparation);
        } else {
            log.info("Email sending disabled in config see `jhipster.mail.enabled`");
        }
        log.info("email send sent!");
    }

    private String createMailTemplate(final String templateFilename, final Map<String, Object> params) {
        Context context = new Context();
        params.forEach(context::setVariable);
        return templateEngine.process(templateFilename, context);
    }

}
