/*
 *     Copyright (C) 2019  Vikas Kumar Verma
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.vedanta.vidiyalay.utility_service.schedulers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.vedanta.vidiyalay.account_service.services.FeeDueRecalculateService;
import org.vedanta.vidiyalay.utility_service.repository.JobSchedulerInformationEntityRepository;

@Service
@Slf4j
public class Scheduler extends AbstractScheduler {
    private final FeeDueRecalculateService feeDueRecalculatorService;

    public Scheduler(JobSchedulerInformationEntityRepository jobSchedulerInformationEntityRepository, FeeDueRecalculateService feeDueRecalculatorService) {
        super(jobSchedulerInformationEntityRepository);
        this.feeDueRecalculatorService = feeDueRecalculatorService;
    }

    @Scheduled(cron = "0 0/30 * * * *") // run every half an hour
    public void scheduleFeeDueAndFineDueRecalculate() {
        scheduleFeeDueAndFineDueRecalculate(feeDueRecalculatorService);
    }
}


// cron expression: url: http://bit.ly/spring-boot-cron-expression
//"0 0 * * * *" = the top of every hour of every day.
//"*/10 * * * * *" = every ten seconds.
//"0 0 8-10 * * *" = 8, 9 and 10 o'clock of every day.
//"0 0 6,19 * * *" = 6:00 AM and 7:00 PM every day.
//"0 0/30 8-10 * * *" = 8:00, 8:30, 9:00, 9:30, 10:00 and 10:30 every day.
//"0 0 9-17 * * MON-FRI" = on the hour nine-to-five weekdays
//"0 0 0 25 12 ?" = every Christmas Day at midnight

