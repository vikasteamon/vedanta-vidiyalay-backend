/*
 *     Copyright (C) 2019  Vikas Kumar Verma
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.vedanta.vidiyalay.utility_service.schedulers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.vedanta.vidiyalay.utility_service.domain.JobSchedulerInformationEntity;
import org.vedanta.vidiyalay.utility_service.repository.JobSchedulerInformationEntityRepository;
import org.vedanta.vidiyalay.utils.CloneObjects;
import org.vedanta.vidiyalay.utils.Utility;

import javax.validation.constraints.NotNull;
import java.util.Map;

@Slf4j
@Service
class AbstractScheduler {
     AbstractScheduler(JobSchedulerInformationEntityRepository jobSchedulerInformationEntityRepository) {
        this.jobSchedulerInformationEntityRepository = jobSchedulerInformationEntityRepository;
    }

    void scheduleFeeDueAndFineDueRecalculate(@NotNull final ScheduledJob job)  {
        JobSchedulerInformationEntity jobDetails = createBatchScheduleEntry(job.getProcessName());
        try {
            job.job();
            log.info(batchFinishSuccessEntry(jobDetails).toString());
        } catch (Exception e) {
            log.error("Error in running job Error: {}, {}", e.getMessage(), jobDetails);
            log.error(batchFinishFailEntry(jobDetails).toString());
        }
    }


    private final JobSchedulerInformationEntityRepository jobSchedulerInformationEntityRepository;

    private JobSchedulerInformationEntity batchFinishFailEntry(@NotNull final JobSchedulerInformationEntity entity){

        return CloneObjects.clone(entity, JobSchedulerInformationEntity.class,
                new CloneObjects.ParameterMap()
                        .put("jobStatus", JobStatus.FAIL).build())
                .map(this::updateBatchStatusEntry)
                .orElse(entity);


    }

    private JobSchedulerInformationEntity batchFinishSuccessEntry(@NotNull final JobSchedulerInformationEntity entity) throws InstantiationException, IllegalAccessException {

        return CloneObjects.clone(entity, JobSchedulerInformationEntity.class,
                new CloneObjects.ParameterMap()
                        .put("jobStatus", JobStatus.SUCCESS).build())
                .map(this::updateBatchStatusEntry)
                .orElse(entity);


    }

    private JobSchedulerInformationEntity updateBatchStatusEntry(@NotNull JobSchedulerInformationEntity entity)  {

        final Map<String, Object> params = new CloneObjects.ParameterMap()
                .put("scheduleStatus", ScheduleStatus.COMPLETED)
                .put("EndTime", Utility.getCurrentDateTime()).build();

        return CloneObjects.clone(entity, JobSchedulerInformationEntity.class,params)
                .map(jobSchedulerInformationEntityRepository::save)
                .orElse(entity);

    }

    private JobSchedulerInformationEntity createBatchScheduleEntry(@NotNull final String processName) {
        return jobSchedulerInformationEntityRepository.save(
                JobSchedulerInformationEntity.builder()
                        .processName(processName)
                        .startTime(Utility.getCurrentDateTime())
                        .scheduleStatus(ScheduleStatus.STARTED)
                        .build());
    }
}
