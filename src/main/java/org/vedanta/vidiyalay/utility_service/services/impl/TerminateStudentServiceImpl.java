package org.vedanta.vidiyalay.utility_service.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.vedanta.vidiyalay.account_service.events.CustomApplicationEvents;
import org.vedanta.vidiyalay.account_service.events.EventType;
import org.vedanta.vidiyalay.account_service.services.AccountService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountMasterVM;
import org.vedanta.vidiyalay.config.ApplicationProperties;
import org.vedanta.vidiyalay.email_service.SendEmailNotification;
import org.vedanta.vidiyalay.email_service.web.rest.vm.EmailVM;
import org.vedanta.vidiyalay.fine_service.domain.FineRecordEntity;
import org.vedanta.vidiyalay.fine_service.domain.enums.Status;
import org.vedanta.vidiyalay.fine_service.service.QueryFineRecordService;
import org.vedanta.vidiyalay.student_service.domain.StudentNewAdmissionEntity;
import org.vedanta.vidiyalay.student_service.domain.enums.AdmissionStatus;
import org.vedanta.vidiyalay.student_service.service.StudentNewAdmissionService;
import org.vedanta.vidiyalay.student_service.service.UpdateStudentDetailsService;
import org.vedanta.vidiyalay.utility_service.services.TerminateStudentService;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Optional;

@Service
@Slf4j
@Validated
public class TerminateStudentServiceImpl implements TerminateStudentService {
    private final AccountService accountService;
    private final StudentNewAdmissionService studentNewAdmissionService;
    private final QueryFineRecordService queryFineRecordService;
    private final UpdateStudentDetailsService updateStudentDetailsService;
    private final SendEmailNotification sendEmailNotification;
    private final ApplicationProperties applicationProperties;

    TerminateStudentServiceImpl(AccountService accountService,
                                StudentNewAdmissionService studentNewAdmissionService,
                                QueryFineRecordService queryFineRecordService,
                                UpdateStudentDetailsService updateStudentDetailsService, SendEmailNotification sendEmailNotification, ApplicationProperties applicationProperties) {
        this.accountService = accountService;
        this.studentNewAdmissionService = studentNewAdmissionService;
        this.queryFineRecordService = queryFineRecordService;
        this.updateStudentDetailsService = updateStudentDetailsService;
        this.sendEmailNotification = sendEmailNotification;
        this.applicationProperties = applicationProperties;
    }

    @Override
    public Optional<AccountMasterVM> getAccountDetailsByEnrolmentNo(@NotNull AccountService accountService,
                                                                    @NotNull final Long enrolmentNo) {
        return Optional.ofNullable(accountService.getAccountDetailsNoError(enrolmentNo));
    }

    @Override
    public void terminateAStudent(@NotNull AccountService accountService,
                                  @NotNull UpdateStudentDetailsService updateStudentDetailsService,
                                  @NotNull final Long enrolmentNo) {
        final AccountMasterVM accountMasterVM = getAccountDetailsByEnrolmentNo(accountService, enrolmentNo)
                .orElse(null);

        if (Objects.isNull(accountMasterVM)) {
            // send warning message or email
            log.warn("Account not found for enrolment No: {}, please ask to create fee account first!", enrolmentNo);
            //send email notification
            final String adminMailAddress = (StringUtils.hasText(applicationProperties.getNotifications().getMailAddress().get("admin")))
                    ? applicationProperties.getNotifications().getMailAddress().get("admin")
                    : applicationProperties.getExceptionHandler().getDeveloperEmail();

            final String subject = String.format("Account not found for Enrolment No '%d'", enrolmentNo);

            sendEmail(subject, subject, adminMailAddress);

            return;
        }

        if (accountMasterVM.getDueAmount().compareTo(BigDecimal.ZERO) > 0) {
            throw new IllegalArgumentException("This student as total Fee Due: " + accountMasterVM.getDueAmount());
        }


        updateStudentDetailsService.terminate_V2(accountMasterVM);
    }

    @Override
    @EventListener(CustomApplicationEvents.class)
    @Async
    public void terminateStudentScheduler(CustomApplicationEvents customApplicationEvents) {
        final String sourceFromEvent = customApplicationEvents.getSourceFromEvent(EventType.PROCESS_TERMINATION_IN_PROGRESS_STUDENTS, String.class);
        if (!(StringUtils.hasText(sourceFromEvent))) {
            return;
        }

        terminateStudentThatTerminationInitiated(accountService,
                studentNewAdmissionService,
                queryFineRecordService,
                updateStudentDetailsService);
    }

    @Override
    public void terminateStudentThatTerminationInitiated(final AccountService accountService,
                                                         final StudentNewAdmissionService studentNewAdmissionService,
                                                         final QueryFineRecordService queryFineRecordService,
                                                         final UpdateStudentDetailsService updateStudentDetailsService) {
        studentNewAdmissionService.findAll()
                .forEach(e ->
                        checkAndTerminateAStudent(accountService, queryFineRecordService, updateStudentDetailsService, e));
    }


    private void checkAndTerminateAStudent(AccountService accountService,
                                           QueryFineRecordService queryFineRecordService,
                                           UpdateStudentDetailsService updateStudentDetailsService,
                                           StudentNewAdmissionEntity studentNewAdmissionEntity) {

        if (studentNewAdmissionEntity.getAdmissionStatus() != AdmissionStatus.TERMINATION_INITIATED) {
            return;
        }

        if (studentNewAdmissionEntity.getIsFeeDue()) {
            final String message = String.format("This student with enrolment no: {}, have fee due, cannot terminate student with " +
                    "due fee.", studentNewAdmissionEntity.getId());
            log.warn(message);
            final String subject = "Terminate Student Failure!";

            //send email notification
            final String adminMailAddress = (StringUtils.hasText(applicationProperties.getNotifications().getMailAddress().get("admin")))
                    ? applicationProperties.getNotifications().getMailAddress().get("admin")
                    : applicationProperties.getExceptionHandler().getDeveloperEmail();

            sendEmail(message, subject, adminMailAddress);

            return;
        }

        @NotNull final BigDecimal fineAmount = calculateTotalFineAmount(queryFineRecordService, studentNewAdmissionEntity.getId());

        if (fineAmount.compareTo(BigDecimal.ZERO) > 0) {
            log.warn("This student with enrolment no: {}, have fine amount: {} due, cannot terminate student with " +
                    "due fee.", studentNewAdmissionEntity.getId(), fineAmount);


            final String message = String.format("This student with enrolment no: {}, have fine amount: {} due, cannot terminate student with " +
                    "due fee.", studentNewAdmissionEntity.getId(), fineAmount);

            final String subject = "Terminate Student Failure!";

            //send email notification
            final String adminMailAddress = (StringUtils.hasText(applicationProperties.getNotifications().getMailAddress().get("admin")))
                    ? applicationProperties.getNotifications().getMailAddress().get("admin")
                    : applicationProperties.getExceptionHandler().getDeveloperEmail();


            sendEmail(message, subject, adminMailAddress);

            return;

        }

        terminateAStudent(accountService, updateStudentDetailsService, studentNewAdmissionEntity.getId());
    }

    private void sendEmail(String message, String subject, String adminMailAddress) {
        final String[] toAddress = (adminMailAddress.contains(","))
                ? adminMailAddress.split(",")
                : new String[]{adminMailAddress};

        sendEmailNotification.sendEmail(EmailVM.builder()
                .subject(subject)
                .text(message)
                .tos(toAddress)
                .build());
    }


    @NotNull
    private static BigDecimal calculateTotalFineAmount(QueryFineRecordService queryFineRecordService,
                                                       Long enrolmentNo) {
        return queryFineRecordService.findByEnrolmentNo(enrolmentNo)
                .stream()
                .filter(x -> x.getStatus() == Status.ACTIVE)
                .map(FineRecordEntity::getAmount)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }

}
