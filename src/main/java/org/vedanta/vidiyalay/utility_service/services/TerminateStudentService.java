package org.vedanta.vidiyalay.utility_service.services;

import org.vedanta.vidiyalay.account_service.events.CustomApplicationEvents;
import org.vedanta.vidiyalay.account_service.services.AccountService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountMasterVM;
import org.vedanta.vidiyalay.fine_service.service.QueryFineRecordService;
import org.vedanta.vidiyalay.student_service.service.StudentNewAdmissionService;
import org.vedanta.vidiyalay.student_service.service.UpdateStudentDetailsService;

import javax.validation.constraints.NotNull;
import java.util.Optional;

public interface TerminateStudentService {

    Optional<AccountMasterVM> getAccountDetailsByEnrolmentNo(@NotNull AccountService accountService,
                                                             @NotNull Long enrolmentNo);


    void terminateAStudent(@NotNull AccountService accountService,
                           @NotNull UpdateStudentDetailsService updateStudentDetailsService,
                           @NotNull Long enrolmentNo);

    void terminateStudentScheduler(CustomApplicationEvents customApplicationEvents);

    void terminateStudentThatTerminationInitiated(AccountService accountService, StudentNewAdmissionService studentNewAdmissionService, QueryFineRecordService queryFineRecordService, UpdateStudentDetailsService updateStudentDetailsService);
}
