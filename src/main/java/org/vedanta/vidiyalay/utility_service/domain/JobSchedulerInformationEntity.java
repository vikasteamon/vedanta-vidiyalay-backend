/*
 *     Copyright (C) 2019  Vikas Kumar Verma
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.vedanta.vidiyalay.utility_service.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.vedanta.vidiyalay.student_service.domain.BaseEntity;
import org.vedanta.vidiyalay.utility_service.schedulers.JobStatus;
import org.vedanta.vidiyalay.utility_service.schedulers.ScheduleStatus;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "job_scheduler_information")
@EqualsAndHashCode(callSuper = true)
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class JobSchedulerInformationEntity extends BaseEntity {
    @Id
    @GeneratedValue
    private Long id;

    @NotNull
    private String processName;
    @NotNull
    private Date startTime;
    private Date endTime;
    @NotNull
    @Enumerated(EnumType.STRING)
    private ScheduleStatus scheduleStatus;
    @Enumerated(EnumType.STRING)
    private JobStatus jobStatus;
}
