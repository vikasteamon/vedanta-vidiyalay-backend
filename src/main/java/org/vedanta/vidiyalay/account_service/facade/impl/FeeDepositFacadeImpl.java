/*
 *     Copyright (C) 2019  Vikas Kumar Verma
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.vedanta.vidiyalay.account_service.facade.impl;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.vedanta.vidiyalay.account_service.facade.FeeDepositFacade;
import org.vedanta.vidiyalay.account_service.facade.QueryFeeDetailFacade;
import org.vedanta.vidiyalay.account_service.services.FeeDepositService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountTransactionVo;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;

import java.math.BigDecimal;

@Component
public class FeeDepositFacadeImpl implements FeeDepositFacade {

    private final QueryFeeDetailFacade queryFeeDetailFacade;

    public FeeDepositFacadeImpl(QueryFeeDetailFacade queryFeeDetailFacade, FeeDepositService feeDepositService) {
        this.queryFeeDetailFacade = queryFeeDetailFacade;
        this.feeDepositService = feeDepositService;
    }

    private final FeeDepositService feeDepositService;

    @Override
    @Transactional(rollbackFor = Throwable.class)
    public BigDecimal depositFee(AccountTransactionVo accountTransactionVo) throws InstantiationException, IllegalAccessException {
        final BigDecimal totalDueFee = queryFeeDetailFacade.getTotalDueFee(accountTransactionVo.getEnrolmentNo());
        final StudentNewAdmissionVM studentDetailsVM = queryFeeDetailFacade.getStudentDetailsVM(accountTransactionVo.getEnrolmentNo());
        return feeDepositService.depositFee(accountTransactionVo, studentDetailsVM, totalDueFee) ;
    }

}
