/*
 *     Copyright (C) 2019  Vikas Kumar Verma
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.vedanta.vidiyalay.account_service.facade.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.vedanta.vidiyalay.config.ApplicationProperties;
import org.vedanta.vidiyalay.email_service.SendEmailNotification;
import org.vedanta.vidiyalay.email_service.web.rest.vm.EmailVM;
import org.vedanta.vidiyalay.email_service.web.rest.vm.exception.PriorityEnum;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;

import java.util.HashMap;
import java.util.Map;

@Service
@Slf4j
public class ExceptionMailer {
    private final SendEmailNotification sendEmailNotification;
    private final ApplicationProperties applicationProperties;

    public ExceptionMailer(SendEmailNotification sendEmailNotification, ApplicationProperties applicationProperties) {
        this.sendEmailNotification = sendEmailNotification;
        this.applicationProperties = applicationProperties;
    }

    public void sendExceptionDetails(StudentNewAdmissionVM studentNewAdmissionVM, String message) {

        if(!applicationProperties.getExceptionHandler().isEnabled()){
            log.info("exception notification disabled. see property `application.exceptionHandler.enabled`.");
            return;
        }
        log.info("Sending exception mail for class: {}, and argument {}");
        Assert.notNull(applicationProperties, "Application property is null");

        sendEmailNotification.sendEmail(EmailVM.builder()
                .subject(String.format("Alert! Priority: %s error found", PriorityEnum.HIGH.name()))
                .templateFile("student-account-failure-notification-mail.html")
                .params(getMailTemplateParams(studentNewAdmissionVM, message))
                .to(applicationProperties.getExceptionHandler().getDeveloperEmail()).build());
    }

    private Map<String, Object> getMailTemplateParams(StudentNewAdmissionVM studentNewAdmissionVM, String message) {
        final Map<String, Object> params = new HashMap<>();
        params.put("name", studentNewAdmissionVM.getName());
        params.put("enrolmentNo", studentNewAdmissionVM.getId());
        params.put("message", message);
        params.put("standard", studentNewAdmissionVM.getAdmissionClass());
        return params;
    }
}
