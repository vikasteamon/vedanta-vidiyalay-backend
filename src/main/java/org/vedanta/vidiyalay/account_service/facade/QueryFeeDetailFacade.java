package org.vedanta.vidiyalay.account_service.facade;

import org.vedanta.vidiyalay.account_service.web.rest.vm.FeeDetailsVM;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface QueryFeeDetailFacade {

    StudentNewAdmissionVM getStudentDetailsVM(Long enrolmentNo);

    BigDecimal getTotalDueFee(Long enrolmentNo);

    List<FeeDetailsVM> getFeeDetails(int standard, int year) ;

    Optional<BigDecimal> getTotalFee(int standard, int year) ;

    void calculateDueFee(final FeeDetailsVM feeDetailsVM) ;
}
