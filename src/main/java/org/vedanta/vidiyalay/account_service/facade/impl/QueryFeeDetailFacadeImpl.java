package org.vedanta.vidiyalay.account_service.facade.impl;

//import org.springframework.cloud.stream.annotation.StreamListener;
//import org.springframework.messaging.handler.annotation.Payload;

import org.springframework.stereotype.Component;
import org.vedanta.vidiyalay.account_service.facade.QueryFeeDetailFacade;
import org.vedanta.vidiyalay.account_service.services.QueryFeeDetailService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.FeeDetailsVM;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Component
public class QueryFeeDetailFacadeImpl implements QueryFeeDetailFacade {
    private final QueryFeeDetailService queryFeeDetailService;

    public QueryFeeDetailFacadeImpl(QueryFeeDetailService queryFeeDetailService) {
        this.queryFeeDetailService = queryFeeDetailService;
    }

    @Override
    public StudentNewAdmissionVM getStudentDetailsVM(Long enrolmentNo) {
        return queryFeeDetailService.getStudentDetailsVM(enrolmentNo);
    }

    @Override
    public BigDecimal getTotalDueFee(Long enrolmentNo) {
        return queryFeeDetailService.getTotalDueFee(enrolmentNo);
    }

    @Override
    public List<FeeDetailsVM> getFeeDetails(int standard, int year) {
        return queryFeeDetailService.getFeeDetails(standard, year);
    }

    @Override
    public Optional<BigDecimal> getTotalFee(int standard, int year) {
        return queryFeeDetailService.getTotalFee(standard, year);
    }

    @Override
//    @StreamListener(FeeMasterUpdateStreams.INPUT)
    public void calculateDueFee(FeeDetailsVM feeDetailsVM) {
        queryFeeDetailService.calculateDueFee(feeDetailsVM);
    }
}
