/*
 *     Copyright (C) 2019  Vikas Kumar Verma
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.vedanta.vidiyalay.account_service.facade.impl;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.vedanta.vidiyalay.account_service.events.CustomApplicationEvents;
import org.vedanta.vidiyalay.account_service.events.EventType;
import org.vedanta.vidiyalay.account_service.facade.AccountFacade;
import org.vedanta.vidiyalay.account_service.facade.QueryFeeDetailFacade;
import org.vedanta.vidiyalay.account_service.services.AccountService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountMasterVM;
import org.vedanta.vidiyalay.student_service.exception.StudentNotFoundException;
import org.vedanta.vidiyalay.student_service.facade.QueryStudentFacade;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;
import org.vedanta.vidiyalay.utils.Utility;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.Objects;

@Component
@Slf4j
@Validated
public class AccountFacadeImpl implements AccountFacade {

    private final AccountService accountService;
    private final QueryFeeDetailFacade queryFeeDetailFacade;
    private final QueryStudentFacade queryStudentFacade;
    private final ApplicationEventPublisher applicationEventPublisher;

    public AccountFacadeImpl(AccountService accountService, QueryFeeDetailFacade queryFeeDetailFacade, QueryStudentFacade queryStudentFacade, ApplicationEventPublisher applicationEventPublisher) {
        this.accountService = accountService;
        this.queryFeeDetailFacade = queryFeeDetailFacade;
        this.queryStudentFacade = queryStudentFacade;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    public AccountMasterVM createNewAccount(@NotNull final AccountMasterVM accountMasterVM) {

        final StudentNewAdmissionVM studentDetails = getStudentDetails(accountMasterVM.getEnrolmentNo());
        final BigDecimal totalFeeForAdmissionClass = getTotalFeeForAdmissionClass(studentDetails.getAdmissionClass());
        return accountService.createNewAccount(studentDetails, totalFeeForAdmissionClass);
    }

    @Override
    public AccountMasterVM updateAccount(AccountMasterVM accountMasterVM) {
        return accountService.updateAccount(accountMasterVM);
    }

//
//    @EventListener(CustomApplicationEvents.class)
//    @Async
//    public void getAccountDetails(final CustomApplicationEvents customApplicationEvents) {
//
//        final Long enrolmentNo = customApplicationEvents.getSourceFromEvent(EventType.STUDENT_TERMINATION_PROCESS_STARTED,
//                Long.class);
//
//        if (Objects.isNull(enrolmentNo)) {
//            return;
//        }
//
//        final AccountMasterVM accountDetails = getAccountDetails(enrolmentNo);
//
//        if (null != accountDetails) {
//            log.debug("Account master data found: {}", accountDetails);
//            applicationEventPublisher.publishEvent(new CustomApplicationEvents(accountDetails, EventType.STUDENT_ACCOUNT_DETAILS_VERIFIED));
//        } else {
//            log.warn("Account master data not found for enrolment no: {}", enrolmentNo);
//        }
//    }

    public AccountMasterVM getAccountDetails(final Long enrolmentNo) {
        return accountService.getAccountDetails(enrolmentNo);
    }


    @EventListener(CustomApplicationEvents.class)
    @Override
    @Async
    public void studentCreatedEventListener(CustomApplicationEvents customApplicationEvents){
        final StudentNewAdmissionVM studentNewAdmissionVM
                = customApplicationEvents.getSourceFromEvent(EventType.STUDENT_CREATED, StudentNewAdmissionVM.class);

        if(Objects.isNull(studentNewAdmissionVM)){
            return;
        }
        createAccountListener(studentNewAdmissionVM);
    }

    @Override
//    @StreamListener(StudentDetailsStreams.INPUT)
    @HystrixCommand(fallbackMethod = "receiveMessage_fallback")
    public void createAccountListener(final StudentNewAdmissionVM studentNewAdmissionVM) {
        final BigDecimal totalFeeForAdmissionClass = getTotalFeeForAdmissionClass(studentNewAdmissionVM.getAdmissionClass());
        accountService.createAccountListener(studentNewAdmissionVM, totalFeeForAdmissionClass);
    }

    private StudentNewAdmissionVM getStudentDetails(Long enrolmentNo) {
        return queryStudentFacade.findByEnrolmentNoAndValidateAdmissionStatus(enrolmentNo)
                .orElseThrow(() -> new StudentNotFoundException(enrolmentNo));
    }

    private BigDecimal getTotalFeeForAdmissionClass(Integer admissionClass) {
        return queryFeeDetailFacade.getTotalFee(admissionClass, Utility.getSessionYear())
                .orElseThrow(() -> new IllegalArgumentException("Fee master record not found"));
    }
}
