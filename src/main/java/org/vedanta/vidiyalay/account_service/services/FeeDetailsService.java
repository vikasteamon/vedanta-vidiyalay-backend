package org.vedanta.vidiyalay.account_service.services;

import org.vedanta.vidiyalay.account_service.web.rest.vm.FeeDetailsVM;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

public interface FeeDetailsService   {

    List<FeeDetailsVM> getFeeDetails(int standard, int year);

    Optional<BigDecimal> getTotalFee(int standard, int year);

    FeeDetailsVM createFeeDetail(FeeDetailsVM feeDetailsVM);
}
