package org.vedanta.vidiyalay.account_service.services;

import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountTransactionVo;
import org.vedanta.vidiyalay.account_service.web.rest.vm.FeeDetailsVM;

import java.math.BigDecimal;

public interface UpdateFeeDetailService {
    BigDecimal calculateAndUpdateFeeDueStatus(AccountTransactionVo accountTransactionVo, BigDecimal totalDueFee) throws IllegalAccessException, InstantiationException;

    FeeDetailsVM createFeeDetail(final FeeDetailsVM feeDetailsVM);

}
