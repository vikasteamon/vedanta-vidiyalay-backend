package org.vedanta.vidiyalay.account_service.services.impl;

import org.springframework.stereotype.Service;
import org.vedanta.vidiyalay.account_service.domain.mapper.FeeDetailsEntityMapper;
import org.vedanta.vidiyalay.account_service.repository.FeesDetailsRepository;
import org.vedanta.vidiyalay.account_service.services.QueryFeesService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.FeeDetailsVM;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class QueryFeesServiceImpl implements QueryFeesService {
    private final FeesDetailsRepository feesDetailsRepository;
    private final FeeDetailsEntityMapper mapper;

    public QueryFeesServiceImpl(FeesDetailsRepository feesDetailsRepository, FeeDetailsEntityMapper mapper) {
        this.feesDetailsRepository = feesDetailsRepository;
        this.mapper = mapper;
    }

    @Override
    public List<FeeDetailsVM> findAll() {
        return feesDetailsRepository.findAll()
                .stream()
                .map(mapper::toVM)
                .collect(Collectors.toList());
    }
}
