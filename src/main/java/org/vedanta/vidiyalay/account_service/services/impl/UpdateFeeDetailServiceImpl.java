package org.vedanta.vidiyalay.account_service.services.impl;

import org.springframework.stereotype.Service;
import org.vedanta.vidiyalay.account_service.domain.mapper.FeeDetailsEntityMapper;
import org.vedanta.vidiyalay.account_service.repository.FeesDetailsRepository;
import org.vedanta.vidiyalay.account_service.services.AccountService;
import org.vedanta.vidiyalay.account_service.services.UpdateFeeDetailService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountMasterVM;
import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountTransactionVo;
import org.vedanta.vidiyalay.account_service.web.rest.vm.FeeDetailsVM;
import org.vedanta.vidiyalay.student_service.exception.StudentNotFoundException;
import org.vedanta.vidiyalay.student_service.service.QueryStudentService;
import org.vedanta.vidiyalay.student_service.service.UpdateStudentDetailsService;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;
import org.vedanta.vidiyalay.utils.CloneObjects;

import java.math.BigDecimal;
import java.util.Optional;

@Service
public class UpdateFeeDetailServiceImpl implements UpdateFeeDetailService {
    private final QueryStudentService queryStudentService;
    private final UpdateStudentDetailsService updateStudentDetailsService;
    private final FeesDetailsRepository feesDetailsRepository;
    private final FeeDetailsEntityMapper mapper;
    private final AccountService accountService;


    public UpdateFeeDetailServiceImpl(QueryStudentService queryStudentService, UpdateStudentDetailsService updateStudentDetailsService, FeesDetailsRepository feesDetailsRepository, FeeDetailsEntityMapper mapper, AccountService accountService) {
        this.queryStudentService = queryStudentService;
        this.updateStudentDetailsService = updateStudentDetailsService;
        this.feesDetailsRepository = feesDetailsRepository;
        this.mapper = mapper;
        this.accountService = accountService;
    }


    @Override
    public BigDecimal calculateAndUpdateFeeDueStatus(AccountTransactionVo accountTransactionVo, final BigDecimal totalDueFee) throws IllegalAccessException, InstantiationException {

        final AccountMasterVM accountDetails = accountService.getAccountDetails(accountTransactionVo.getEnrolmentNo());
        // update new due amount in account master
        final BigDecimal balanceAmount = updateNewDueAmountInMaster(accountDetails,
                accountTransactionVo.getAmount(), totalDueFee);

        // if no fee due then update fee dew flag in student details
        updateFeeDueStatus(accountTransactionVo.getEnrolmentNo(), balanceAmount, accountDetails.getTotalFine());
        return balanceAmount;
    }


    private BigDecimal updateNewDueAmountInMaster(final AccountMasterVM accountDetails, final BigDecimal depositAmount,
                                                  final BigDecimal totalDueFee) throws InstantiationException, IllegalAccessException {
        // calculate new due fee
        final BigDecimal balanceAmount = calculateBalanceAmount(depositAmount, totalDueFee);

        CloneObjects.clone(accountDetails, AccountMasterVM.class,
                new CloneObjects.ParameterMap().put("dueAmount", balanceAmount).build())
                .map(accountService::updateAccount);

        return balanceAmount;
    }

    private BigDecimal calculateBalanceAmount(final BigDecimal depositAmount, BigDecimal totalDueFee) {
        // calculate new due fee
        return totalDueFee.subtract(depositAmount);
    }


    private void updateFeeDueStatus(final Long enrolmentNo, final BigDecimal balanceAmount, final BigDecimal totalFineAmount) {
        final StudentNewAdmissionVM studentNewAdmissionVM = getStudentDetailsVM(enrolmentNo);

        studentNewAdmissionVM.setIsFeeDue(balanceAmount.compareTo(BigDecimal.ZERO) > 0 ||
                totalFineAmount.compareTo(BigDecimal.ZERO) > 0);

        Optional.ofNullable(updateStudentDetailsService.update(studentNewAdmissionVM))
                .orElseThrow(() -> new RuntimeException("did not update student data."));

    }

    private StudentNewAdmissionVM getStudentDetailsVM(Long enrolmentNo) {
        return queryStudentService.findByEnrolmentNoAndValidateAdmissionStatus(enrolmentNo)
                .orElseThrow(() -> new StudentNotFoundException(enrolmentNo));
    }

    @Override
    public FeeDetailsVM createFeeDetail(FeeDetailsVM feeDetailsVM) {
//         create new fee details records
        return mapper.toVM(feesDetailsRepository.save(mapper.toEntity(feeDetailsVM)));
    }
}
