package org.vedanta.vidiyalay.account_service.services;

import org.vedanta.vidiyalay.account_service.web.rest.vm.FeeDetailsVM;

import java.util.List;

public interface QueryFeesService {
    List<FeeDetailsVM> findAll();
}
