package org.vedanta.vidiyalay.account_service.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.vedanta.vidiyalay.account_service.domain.AccountMasterEntity;
import org.vedanta.vidiyalay.account_service.domain.FeeDetailsEntity;
import org.vedanta.vidiyalay.account_service.domain.mapper.FeeDetailsEntityMapper;
import org.vedanta.vidiyalay.account_service.repository.AccountMasterRepository;
import org.vedanta.vidiyalay.account_service.repository.FeesDetailsRepository;
import org.vedanta.vidiyalay.account_service.services.AccountService;
import org.vedanta.vidiyalay.account_service.services.QueryFeeDetailService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountMasterVM;
import org.vedanta.vidiyalay.account_service.web.rest.vm.FeeDetailsVM;
import org.vedanta.vidiyalay.student_service.domain.enums.AdmissionStatus;
import org.vedanta.vidiyalay.student_service.exception.StudentNotFoundException;
import org.vedanta.vidiyalay.student_service.service.QueryStudentService;
import org.vedanta.vidiyalay.student_service.service.StudentNewAdmissionService;
import org.vedanta.vidiyalay.student_service.service.UpdateStudentDetailsService;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@Slf4j
public class QueryFeeDetailServiceImpl implements QueryFeeDetailService {
    private final QueryStudentService queryStudentService;
    private final UpdateStudentDetailsService updateStudentDetailsService;
    private final StudentNewAdmissionService studentNewAdmissionService;
    private final AccountMasterRepository accountMasterRepository;
    private final FeesDetailsRepository feesDetailsRepository;
    private final FeeDetailsEntityMapper mapper;
    private final AccountService accountService;

    public QueryFeeDetailServiceImpl(QueryStudentService queryStudentService, UpdateStudentDetailsService updateStudentDetailsService, StudentNewAdmissionService studentNewAdmissionService, AccountMasterRepository accountMasterRepository, FeesDetailsRepository feesDetailsRepository, FeeDetailsEntityMapper mapper, AccountService accountService) {
        this.queryStudentService = queryStudentService;
        this.updateStudentDetailsService = updateStudentDetailsService;
        this.studentNewAdmissionService = studentNewAdmissionService;
        this.accountMasterRepository = accountMasterRepository;
        this.feesDetailsRepository = feesDetailsRepository;
        this.mapper = mapper;
        this.accountService = accountService;
    }

    @Override
    public void calculateDueFee(final FeeDetailsVM feeDetailsVM) {

        //get all the students for that class year from student details
        final List<StudentNewAdmissionVM> studentNewAdmissionVMS =
                studentNewAdmissionService.findByParameters(null, feeDetailsVM.getStandard(), feeDetailsVM.getYear(),
                        null, null, null, null);

        final BigDecimal amount = getNewFeeAmount(feeDetailsVM);

        final Predicate<StudentNewAdmissionVM> studentNewAdmissionVMPredicate =
                e -> e.getAdmissionStatus() != AdmissionStatus.TERMINATED &&
                        e.getAdmissionStatus() != AdmissionStatus.TERMINATION_INITIATED;

        studentNewAdmissionVMS
                .stream()
                .filter(studentNewAdmissionVMPredicate)
                .forEach(e -> {
                    //get due fee from account master
                        AccountMasterVM accountDetails = accountService.getAccountDetailsNoError(e.getId());

                        if(accountDetails != null){
                            //if new fee is active then add amount to due fee and update account master
                            //if new fee is not active then subtract amount to due fee and update account master

                            BigDecimal newDueAmount = accountDetails.getDueAmount().add(amount);
                            accountService.updateAccount(accountDetails);

                            //if due amount is 0 then update student fee due status to false in student details
                            //else update student fee due status to true
                            updateFeeDueStatus(accountDetails.getEnrolmentNo(), newDueAmount, accountDetails.getTotalFine());

                        } else {
                            log.warn("Account details not found for enrolment: {}", e.getId());
                        }


                });
    }

    private BigDecimal getNewFeeAmount(FeeDetailsVM feeDetailsVM) {
        return feeDetailsVM.getIsActive() ? feeDetailsVM.getAmount() :
                feeDetailsVM.getAmount().multiply(BigDecimal.valueOf(-1L));
    }

    private void updateFeeDueStatus(final Long enrolmentNo, final BigDecimal balanceAmount, final BigDecimal totalFineAmount) {
        final StudentNewAdmissionVM studentNewAdmissionVM = getStudentDetailsVM(enrolmentNo);

        studentNewAdmissionVM.setIsFeeDue(balanceAmount.compareTo(BigDecimal.ZERO) > 0 ||
                totalFineAmount.compareTo(BigDecimal.ZERO) > 0);

        Optional.ofNullable(updateStudentDetailsService.update(studentNewAdmissionVM))
                .orElseThrow(() -> new RuntimeException("did not update student data."));

    }

    public StudentNewAdmissionVM getStudentDetailsVM(Long enrolmentNo) {
        return queryStudentService.findByEnrolmentNoAndValidateAdmissionStatus(enrolmentNo)
                .orElseThrow(() -> new StudentNotFoundException(enrolmentNo));
    }

    @Override
    public BigDecimal getTotalDueFee(Long enrolmentNo) {

        return Optional.ofNullable(accountMasterRepository.findByEnrolmentNo(enrolmentNo))
                .map(AccountMasterEntity::getDueAmount)
                .orElseThrow(() -> new RuntimeException("Account not found for enrolment no:" + enrolmentNo + " !"));
    }

    @Override
    public List<FeeDetailsVM> getFeeDetails(int standard, int year) {
        return Optional.ofNullable(
                feesDetailsRepository.findAllByStandardAndYear(standard, year))
                .map(e -> e.stream()
                        .filter(FeeDetailsEntity::getIsActive)
                        .map(mapper::toVM)
                        .collect(Collectors.toList()))
                .orElse(Collections.emptyList());
    }

    @Override
    public Optional<BigDecimal> getTotalFee(int standard, int year) {

        List<FeeDetailsVM> feeDetails = getFeeDetails(standard, year);
        if (feeDetails.isEmpty()) {
            throw new IllegalArgumentException("Fee details not configured for standard " + standard + " year " + year);
        }

        BigDecimal totalAmount = feeDetails.stream().map(FeeDetailsVM::getAmount)
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        return Optional.of(totalAmount);
    }
}
