/*
 *     Copyright (C) 2019  Vikas Kumar Verma
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.vedanta.vidiyalay.account_service.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.castor.core.util.Assert;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.vedanta.vidiyalay.account_service.domain.mapper.AccountEntityMapper;
import org.vedanta.vidiyalay.account_service.enums.AccountType;
import org.vedanta.vidiyalay.account_service.repository.AccountMasterRepository;
import org.vedanta.vidiyalay.account_service.services.AccountService;
import org.vedanta.vidiyalay.account_service.web.rest.vm.AccountMasterVM;
import org.vedanta.vidiyalay.email_service.SendEmailNotification;
import org.vedanta.vidiyalay.email_service.web.rest.vm.EmailVM;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;
import org.vedanta.vidiyalay.utils.Utility;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Service
@Slf4j
@Validated
public class AccountServiceImpl implements AccountService {

    private final AccountMasterRepository accountMasterRepository;
    private final AccountEntityMapper accountEntityMapper;
    private final SendEmailNotification sendEmailNotification;

    public AccountServiceImpl(AccountMasterRepository accountMasterRepository, AccountEntityMapper accountEntityMapper, SendEmailNotification sendEmailNotification) {
        this.accountMasterRepository = accountMasterRepository;
        this.accountEntityMapper = accountEntityMapper;
        this.sendEmailNotification = sendEmailNotification;
    }

    @Override
    public AccountMasterVM createNewAccount(StudentNewAdmissionVM studentNewAdmissionVM, BigDecimal totalFeeForAdmissionClass) {
        return createAccountEngine(studentNewAdmissionVM, totalFeeForAdmissionClass);
    }


    @Override
    public AccountMasterVM updateAccount(AccountMasterVM accountMasterVM) {
        Assert.notNull(accountMasterVM.getId(),
                String.format("Account Id is blank for enrolment: %s cannot update record", accountMasterVM.getEnrolmentNo()));
        return saveNewAccountMasterData(accountMasterVM, "Cannot process, try again letter.");
    }

    @Override
    public AccountMasterVM getAccountDetails(final Long enrolmentNo) {
        return Optional.ofNullable(accountMasterRepository.findByEnrolmentNo(enrolmentNo))
                .map(accountEntityMapper::toVm)
                .orElseThrow(() -> new RuntimeException("Account not found for enrolment: " + enrolmentNo));
    }


    @Override
    public AccountMasterVM getAccountDetailsNoError(final Long enrolmentNo) {
        return Optional.ofNullable(accountMasterRepository.findByEnrolmentNo(enrolmentNo))
                .map(accountEntityMapper::toVm)
                .orElse(null);
    }

    @Override
    public void createAccountListener(final StudentNewAdmissionVM studentNewAdmissionVM, BigDecimal totalFeeForAdmissionClass) {
        log.info("received message: {}", studentNewAdmissionVM);

        final AccountMasterVM resultEntity = createAccountEngine(studentNewAdmissionVM, totalFeeForAdmissionClass);

        log.debug("Account Master: {}", resultEntity);

        String[] tos = {studentNewAdmissionVM.getEmail()};

        sendEmailNotification.sendEmail(EmailVM.builder()
                .subject("your fee is due")
                .text("Fee due reminder: " + resultEntity.getDueAmount())
                .params(getMailTemplateParams(studentNewAdmissionVM, resultEntity.getTotalFee()))
                .templateFile("new-admission-fee-due-reminder-mail.html")
                .tos(tos).build()
        );
    }



    private AccountMasterVM saveNewAccountMasterData(AccountMasterVM newAccountMasterVM, String s) {
        return Optional.of(accountMasterRepository
                .save(accountEntityMapper.toEntity(newAccountMasterVM)))
                .map(accountEntityMapper::toVm)
                .orElseThrow(() -> new RuntimeException(s));
    }

    private AccountMasterVM getNewAccountMasterVM(StudentNewAdmissionVM studentNewAdmissionVM, BigDecimal totalDueFee) {
        return AccountMasterVM.builder()
                .totalFee(totalDueFee)
                .dueAmount(totalDueFee)
                .accountType(AccountType.STUDENT)
                .dateOfOpening(Utility.getCurrentDateTime()).enrolmentNo(studentNewAdmissionVM.getId())
                .name(studentNewAdmissionVM.getName())
                .totalFine(BigDecimal.ZERO).build();
    }

    private void throwErrorIfAccountAlreadyExists(StudentNewAdmissionVM studentNewAdmissionVM) {
        if(Optional.ofNullable(accountMasterRepository.findByEnrolmentNo(studentNewAdmissionVM.getId()))
                .isPresent()){
            throw new IllegalArgumentException("Account already exists.");
        }
    }
//
//    private StudentNewAdmissionVM getStudentDetails(Long enrolmentNo) {
//        return queryStudentService.findByEnrolmentNoAndValidateAdmissionStatus(enrolmentNo)
//                .orElseThrow(() -> new StudentNotFoundException(enrolmentNo));
//    }

    private AccountMasterVM createAccountEngine(final StudentNewAdmissionVM studentNewAdmissionVM, BigDecimal totalFeeForAdmissionClass) {

        // check if account already exists
        throwErrorIfAccountAlreadyExists(studentNewAdmissionVM);

        final AccountMasterVM newAccountMasterVM = getNewAccountMasterVM(studentNewAdmissionVM, totalFeeForAdmissionClass);

        return saveNewAccountMasterData(newAccountMasterVM, "cannot create account");
    }

    private Map<String, Object> getMailTemplateParams(StudentNewAdmissionVM studentNewAdmissionVM, final BigDecimal dueFee) {
        final Map<String, Object> params = new HashMap<>();
        params.put("fatherName", studentNewAdmissionVM.getFatherName());
        params.put("name", studentNewAdmissionVM.getName());
        params.put("enrolmentNo", studentNewAdmissionVM.getId());
        params.put("standard", studentNewAdmissionVM.getAdmissionClass());
        params.put("amount", dueFee);
        return params;
    }

}

