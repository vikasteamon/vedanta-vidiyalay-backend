/*
 *     Copyright (C) 2019  Vikas Kumar Verma
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.vedanta.vidiyalay.account_service.services.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.vedanta.vidiyalay.account_service.domain.AccountMasterEntity;
import org.vedanta.vidiyalay.account_service.events.CustomApplicationEvents;
import org.vedanta.vidiyalay.account_service.events.EventType;
import org.vedanta.vidiyalay.account_service.repository.AccountMasterRepository;
import org.vedanta.vidiyalay.account_service.repository.AccountTransactionRepository;
import org.vedanta.vidiyalay.account_service.services.FeeDueRecalculateService;
import org.vedanta.vidiyalay.account_service.services.QueryFeeDetailService;
import org.vedanta.vidiyalay.fine_service.domain.FineRecordEntity;
import org.vedanta.vidiyalay.fine_service.domain.enums.Status;
import org.vedanta.vidiyalay.fine_service.service.QueryFineRecordService;
import org.vedanta.vidiyalay.student_service.domain.StudentNewAdmissionEntity;
import org.vedanta.vidiyalay.student_service.domain.enums.AdmissionStatus;
import org.vedanta.vidiyalay.student_service.exception.StudentNotFoundException;
import org.vedanta.vidiyalay.student_service.repository.StudentNewAdmissionEntityRepository;
import org.vedanta.vidiyalay.student_service.service.QueryStudentService;
import org.vedanta.vidiyalay.student_service.service.UpdateStudentDetailsService;
import org.vedanta.vidiyalay.student_service.web.rest.vm.StudentNewAdmissionVM;
import org.vedanta.vidiyalay.utils.CloneObjects;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;
import java.util.stream.IntStream;

@Service
@Slf4j
public class FeeDueRecalculatorServiceImpl implements FeeDueRecalculateService {
    private final AccountMasterRepository accountMasterRepository;
    private final AccountTransactionRepository accountTransactionRepository;
    private final StudentNewAdmissionEntityRepository studentNewAdmissionEntityRepository;
    private final QueryStudentService queryStudentService;
    private final UpdateStudentDetailsService updateStudentDetailsService;
    private final QueryFineRecordService queryFineRecordService;
    private final QueryFeeDetailService queryFeeDetailService;
    private final ApplicationEventPublisher applicationEventPublisher;

    public FeeDueRecalculatorServiceImpl(AccountMasterRepository accountMasterRepository, AccountTransactionRepository accountTransactionRepository, StudentNewAdmissionEntityRepository studentNewAdmissionEntityRepository, QueryStudentService queryStudentService, UpdateStudentDetailsService updateStudentDetailsService, QueryFineRecordService queryFineRecordService, QueryFeeDetailService queryFeeDetailService, ApplicationEventPublisher applicationEventPublisher) {
        this.accountMasterRepository = accountMasterRepository;
        this.accountTransactionRepository = accountTransactionRepository;
        this.studentNewAdmissionEntityRepository = studentNewAdmissionEntityRepository;
        this.queryStudentService = queryStudentService;
        this.updateStudentDetailsService = updateStudentDetailsService;
        this.queryFineRecordService = queryFineRecordService;
        this.queryFeeDetailService = queryFeeDetailService;
        this.applicationEventPublisher = applicationEventPublisher;
    }

    @Override
    @Async
    public CompletableFuture<String> job() {
        // get total fees based on class.<FOR ALL THE CLASS 1-12>
        Map<Integer, BigDecimal> feeDetails = getFeeDetails();
        // get total fee payed for this year for that student <FOR ALL THE ACCOUNTS>

        for (AccountMasterEntity e : accountMasterRepository.findAll()) {
            calculateDueFeeForAStudent(feeDetails, e);
        }

        // create events to initialize terminate student process
        applicationEventPublisher.publishEvent(new CustomApplicationEvents("None", EventType.PROCESS_TERMINATION_IN_PROGRESS_STUDENTS));
        return CompletableFuture.completedFuture("Process Completed!");
    }

    public void calculateDueFeeForAStudent(Map<Integer, BigDecimal> feeDetails, AccountMasterEntity e) {
        Supplier<StudentNotFoundException> studentNotFoundExceptionSupplier = () -> new StudentNotFoundException(e.getEnrolmentNo());

        final StudentNewAdmissionEntity studentNewAdmissionEntity =
                studentNewAdmissionEntityRepository.findById(e.getEnrolmentNo())
                        .orElseThrow(studentNotFoundExceptionSupplier);

        if (studentNewAdmissionEntity.getAdmissionStatus().equals(AdmissionStatus.TERMINATED)) {
            return;
        }

        @NotNull(message = "{error.field.mandatory}") final Integer admissionClass
                = studentNewAdmissionEntity.getAdmissionClass();

        Optional<BigDecimal> paidFee = accountTransactionRepository.getSumOfFeePaid(e.getEnrolmentNo());

        BigDecimal totalFee = feeDetails.get(admissionClass);
        BigDecimal feeDue = totalFee.subtract(paidFee.orElse(BigDecimal.ZERO));

        final @NotNull BigDecimal totalFine = calculateTotalFineAmount(e);

        final Map<String, Object> params = new CloneObjects.ParameterMap()
                .put("dueAmount", feeDue)
                .put("totalFee", totalFee)
                .put("totalFine", totalFine).build();

        Objects.requireNonNull(CloneObjects.clone(e, AccountMasterEntity.class, params))
                .map(accountMasterRepository::save);

        // update due status in student detail table.
        final StudentNewAdmissionVM studentNewAdmissionVM = queryStudentService.findByEnrolmentNoAndValidateAdmissionStatus(e.getEnrolmentNo())
                .orElseThrow(studentNotFoundExceptionSupplier);

        studentNewAdmissionVM.setIsFeeDue(isFeeDue(e, feeDue));

        updateStudentDetailsService.update(studentNewAdmissionVM);
    }

    @NotNull
    private BigDecimal calculateTotalFineAmount(AccountMasterEntity e) {
        return queryFineRecordService.findByEnrolmentNo(e.getEnrolmentNo())
                .stream()
                .filter(x -> x.getStatus() == Status.ACTIVE)
                .map(FineRecordEntity::getAmount)
                .reduce(BigDecimal::add)
                .orElse(BigDecimal.ZERO);
    }

    private boolean isFeeDue(AccountMasterEntity e, BigDecimal feeDue) {
        return (!(feeDue.compareTo(BigDecimal.ZERO) <= 0 &&
                e.getTotalFine().compareTo(BigDecimal.ZERO) <= 0));
    }


    private Map<Integer, BigDecimal> getFeeDetails() {
        Map<Integer, BigDecimal> feeDetails = new HashMap<>();
        IntStream.range(1, 13)
                .forEach(e -> feeDetails.put(e, getTotalFee(e)));
        return feeDetails;
    }

    private BigDecimal getTotalFee(int e) {
        BigDecimal value = BigDecimal.ZERO;
        try {
            value = queryFeeDetailService.getTotalFee(e, 2019).orElse(BigDecimal.ZERO);
        } catch (Exception exp) {
            log.error(exp.getMessage());
        }
        return value;
    }

    @Override
    public String getProcessName() {
        return "FEE_RECALCULATOR";
    }


}
