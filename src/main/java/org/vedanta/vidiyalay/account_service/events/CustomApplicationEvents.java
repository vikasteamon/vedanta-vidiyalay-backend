package org.vedanta.vidiyalay.account_service.events;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

@Getter
public class CustomApplicationEvents extends ApplicationEvent {
    private final EventType eventType;

    public CustomApplicationEvents(Object source, EventType eventType) {
        super(source);
        this.eventType = eventType;
    }

    public <T> T getSourceFromEvent(EventType eventType, Class<T> type) {

        if (this.eventType != eventType) {
            return null;
        }
        final Object source = getSource();
        return (type.isInstance(source)) ? type.cast(source): null;
    }
}

