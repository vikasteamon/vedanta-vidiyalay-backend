/*
 *     Copyright (C) 2019  Vikas Kumar Verma
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU Affero General Public License as
 *     published by the Free Software Foundation, either version 3 of the
 *     License, or (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU Affero General Public License for more details.
 *
 *     You should have received a copy of the GNU Affero General Public License
 *     along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.vedanta.vidiyalay.config.kafka;

//import org.springframework.cloud.stream.annotation.Input;
//import org.springframework.cloud.stream.annotation.Output;
//import org.springframework.messaging.MessageChannel;
//import org.springframework.messaging.SubscribableChannel;

import org.springframework.stereotype.Component;

public interface StudentDetailsStreams {
    String INPUT = "student_details_input";
    String OUTPUT = "student_details_output";

//    @Input(INPUT)
//    SubscribableChannel inboudStudentDetails();
//
//    @Output(OUTPUT)
//    MessageChannel outboundStudentDetails();
}

@Component
class StudentDetailsStreamsMock implements StudentDetailsStreams {

}
